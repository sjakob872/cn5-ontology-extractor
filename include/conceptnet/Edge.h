#pragma once

#include "conceptnet/Relations.h"

#include <string>
#include <vector>

namespace conceptnet
{
class Concept;
class Edge
{
public:
    Edge(std::string id, std::string language, Concept* fromConcept, Concept* toConcept, Relation relation, double weight);
    virtual ~Edge() = default;

    Concept* getOpposite(Concept* c) const;
    std::string toString(const std::string& indent = "") const;
    bool operator<(const Edge& another) const;

    std::string id;
    std::string language;
    long aspId;
    long timeStamp;

    Concept* fromConcept;
    Concept* toConcept;
    Relation relation;
    double weight;
    double level;
    double relatedness;
    bool causesInconsistency;

};
} // namespace conceptnet

bool operator==(const conceptnet::Edge& one, const conceptnet::Edge& another);

