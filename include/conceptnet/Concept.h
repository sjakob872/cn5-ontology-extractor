#pragma once

#include "conceptnet/Relations.h"

#include <string>
#include <vector>

namespace conceptnet
{
class Edge;
class Concept
{
public:
    Concept(std::string id, std::string term, std::string senseLabel);
    virtual ~Concept() = default;

    std::string id;
    std::string term;
    std::string senseLabel;

    std::vector<Edge*> getEdges();
    void addEdge(Edge* edge);
    void addEdges(const std::vector<Edge*>& edgesToAdd);
    void removeEdge(Edge* edge);
    std::vector<Concept*> getConnectedConcepts(Relation relation, bool includeEquivalents);
    std::vector<Concept*> getConnectedConcepts(const std::vector<Relation>& rels, bool includeEquivalents);

private:
    std::vector<Edge*> edges;
};

} // namespace conceptnet
bool operator==(const conceptnet::Concept& one, const conceptnet::Concept& another);