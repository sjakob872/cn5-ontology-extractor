#pragma once

#include <vector>
#include <string>

namespace conceptnet
{
class Edge;
class Concept;
class ConceptPath
{
public:
    explicit ConceptPath(Concept* end);
    ConceptPath(const ConceptPath& other);
    ~ConceptPath() = default;

    void addToPath(Edge* edge);
    void calculateUtility();
    bool containsNode(Concept* c);
    Concept* getEnd() const;
    std::vector<Edge*> getPath() const;
    std::string toString(const std::string& indent = "") const;

    double getUtility() const;

private:
    Concept* end;
    std::vector<Edge*> path;
    double utility;
    static double getEdgeWeight(Edge* edge);

};
} // namespace conceptnet
