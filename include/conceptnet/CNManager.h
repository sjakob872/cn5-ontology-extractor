#pragma once

#include "conceptnet/Relations.h"

#include <map>

namespace conceptnet
{
class Concept;
class Edge;
class CNManager
{
public:
    virtual Concept* getConcept(std::string conceptId) const = 0;
    virtual Concept* createConcept(std::string conceptId, std::string term, std::string senseLabel) = 0;

    virtual Edge* getEdge(std::string edgeId) const = 0;
    virtual Edge* createEdge(std::string edgeId, std::string language, Concept* fromConcept, Concept* toConcept, Relation relation, double weight) = 0;

private:
    std::map<std::string, Concept*> concepts;
    std::map<std::string, Edge*> edges;
};
} // namespace conceptnet