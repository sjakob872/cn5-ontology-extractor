#pragma once
#include <string>

namespace asp
{
class OntologyTerms
{
public:
    static const std::string CONCEPTNET_PREFIX;
    static const std::string SUB_SET_OF;
    static const std::string SUB_SET_OF_INTERNAL;
    static const std::string CLASSIFIED_AS;
    static const std::string CLASSIFIED_AS_INTERNAL;
    static const std::string PROGRAM_SECTION;
    static const std::string INPUT;
    static const std::string FROM_CONCEPT;
    static const std::string TO_CONCEPT;
    static const std::string CONCEPT;
    static const std::string PARENT_CONCEPT;
    static const std::string INTER_CONCEPT;
    static const std::string WEIGHT_VAR;
    static const std::string WEIGHT1_VAR;
    static const std::string WEIGHT2_VAR;
    static const std::string WEIGHT;
    static const std::string MAX_WEIGHT;
    static const std::string MAX_TIMESTEP;
    static const std::string TIMESTEP;
    static const std::string CURRENT_WEIGHT;
    static const std::string ID;
    static const std::string RELATION;
    static const std::string PROPERTY;
    static const std::string HAS_PROPERTY;
    static const std::string PROPERTY_INHERITED_FROM;
    static const std::string FACET;
    static const std::string HAS_FACET;
    static const std::string FACET_INHERITED_FROM;
    static const std::string FACET_OF;
    static const std::string CHILD_PROPERTY;
    static const std::string PARENT_PROPERTY;
    static const std::string SUB_CLASS_OF;
    static const std::string SUB_PROPERTY_OF;
    static const std::string HAS_PROPERTY_VALUE;
    static const std::string INDIVIDUAL;
    static const std::string VALUE;
    static const std::string HAS_VALUE;
    static const std::string HAS_Domain;
    static const std::string DOMAIN;
    static const std::string DOMAIN_OF;
    static const std::string DOMAIN_INHERITED_FROM;
};
} // namespace asp
