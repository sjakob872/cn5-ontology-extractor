#pragma once

#include <map>
#include <string>

namespace conceptnet
{
class Edge;
class Concept;
} // namespace conceptnet
namespace cnoe
{
class AnswerGraph;
class Ontology;
} // namespace cnoe
namespace asp
{
class ASPTranslator
{
public:
    enum InconsistencyRemoval
    {
        None,
        KeepHighestWeight,
        UseExternals
    };
    ASPTranslator();
    static cnoe::Ontology* extractOntology(cnoe::AnswerGraph* answerGraph, bool createExternals = false);
    static void extractOntology(cnoe::AnswerGraph* answerGraph, cnoe::Ontology* ontology, bool createExternals = false);

    std::string generateOntologyRules(bool addShow);
    static void translateEdge(cnoe::AnswerGraph *answerGraph, cnoe::Ontology* ontology, conceptnet::Edge* edge, bool createExternals = false);
    static void translateEdges(cnoe::AnswerGraph *answerGraph, cnoe::Ontology* ontology, bool createExternals = false);

private:
    static std::string conceptToASPPredicate(std::string conceptName);
    static void createOntology(cnoe::AnswerGraph *answerGraph, cnoe::Ontology* ontology, bool createExternals = false, bool newOntology = true);
    void generateSupportOntologyRules(std::string& program);
    void generateSubSetRules(std::string& program);
    void generateClassifiedRules(std::string& program);
    void generatePropertyRules(std::string& program);
    static void generateDomainRules(std::string& program);
    static void generateFacetRules(std::string& program);
    static void generateShowStatements(std::string& program);

    std::string lowerIsARelation;
    std::string lowerFormOfRelation;
    std::string lowerSynonymRelation;
    std::string lowerHasPropertyRelation;
};

} // namespace asp