#pragma once

#include <cgraph.h>
#include <string>
#include <vector>

//#define ONTOLOGY_DEBUG

namespace asp
{
class ASPTranslator;
}
namespace conceptnet
{
class Concept;
class ConceptNet;
} // namespace conceptnet
namespace cnoe
{
class Ontology;
class AnswerGraph;
class OntologyExtractor
{
public:
    OntologyExtractor();
    ~OntologyExtractor();

    void extract(const std::string& rootConcept = "thing");
    void extractOntologyFromString(const std::string& ontology);
    void createEmptyOntology();
    long addEdge(std::string fromConceptString, std::string relation, std::string toConceptString, double weight, long id = -1);
    void deleteEdge(std::string fromConceptString, std::string relation, std::string toConceptString, long id);
    void editEdge(std::string oldStartConcept, std::string newStartConcept, std::string oldRelation, std::string newRelation,
            std::string oldEndConcept, std::string newEndConcept, double weight, long id);
    conceptnet::Concept* createConcept(std::string conceptName);

    static void renderDot(AnswerGraph* answerGraph, const std::string& domainFolder);
    static std::string readFromFile(const std::string& path);
    static std::string generateEdgeId(
            const std::string& fromConceptString, const std::string& relation, const std::string& toConceptString);

    void extractOntology(const std::string& input);
    void generateASPOntology();
    static void generateOntologyGraph(Agraph_t* g, const std::string& atom);

    AnswerGraph* getAnswerGraph() const;
    Ontology* getOntology() const;
    std::vector<std::string> getOntologyRelations();

    bool isFinishedExtracting() const;
    double getIsAWeight() const;
    void setIsAWeight(double isAWeight);
    double getFormOfWeight() const;
    void setFormOfWeight(double formOfWeight);
    double getSynonymWeight() const;
    void setSynonymWeight(double synonymWeight);
    double getMinRelatedness() const;
    void setMinRelatedness(double minRelatedness);
    double getHasPropertyWeight() const;
    void setHasPropertyWeight(double hasPropertyWeight);

    std::vector<int> getStatistics();

    std::vector<std::string> solve(std::string individuals, std::pair<std::string, std::string> facets, std::string solutionPath);

private:
    static std::vector<std::string> getLines(std::string ontology);
    std::string trim(const std::string& str);
    double IS_A_WEIGHT;
    double FORM_OF_WEIGHT;
    double SYNONYM_WEIGHT;
    double MIN_RELATEDNESS;
    double HAS_PROPERTY_WEIGHT;
    asp::ASPTranslator* translator;
    conceptnet::ConceptNet* conceptNet;
    cnoe::AnswerGraph* answerGraph;
    cnoe::Ontology* ontology;
    bool finishedExtracting;
    std::vector<int> statisitcs;
};
} // namespace cnoe
