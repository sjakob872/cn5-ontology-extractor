#pragma once

#include "conceptnet/CNManager.h"

#include <cgraph.h>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace conceptnet
{
class Concept;
class Edge;
class ConceptPath;
} // namespace conceptnet
namespace cnoe
{
class AnswerGraph : public conceptnet::CNManager
{
public:
    AnswerGraph();

    virtual ~AnswerGraph();

    conceptnet::Concept* root;
    std::vector<conceptnet::Concept*> answerConcepts;
    std::vector<conceptnet::ConceptPath*> answerPaths;
    std::map<conceptnet::Concept*, std::vector<conceptnet::Edge*>> adjectiveAntonymMap;
    std::set<conceptnet::Concept*> closedProperties;

    std::string toString() const;
    void renderDot(Agraph_t* g, bool markInconsistencies = false);
    void calculateUtilities();
    std::vector<conceptnet::Concept*> getBestAnswers(size_t maxNumberOfAnswers);
    void setRoot(conceptnet::Concept* root);
    conceptnet::Concept* getConcept(std::string conceptId) const override;
    conceptnet::Concept* createConcept(std::string conceptId, std::string term, std::string senseLabel) override;
    conceptnet::Edge* getEdge(std::string edgeId) const override;
    std::vector<conceptnet::Edge*> getEdges(conceptnet::Concept* firstConcept, conceptnet::Concept* secondConcept);
    conceptnet::Edge* createEdge(std::string edgeId, std::string language, conceptnet::Concept* fromConcept, conceptnet::Concept* toConcept,
                                 conceptnet::Relation relation, double weight) override;
    const std::map<std::string, conceptnet::Concept*>& getConcepts() const;
    const std::map<std::string, conceptnet::Edge*>& getEdges() const;
    void removeEdge(std::string fromConceptString, std::string relation, std::string toConceptString);
    void markInconsistentEdges();

    long getNextASPID();
    void updateASPID(long id);
    void resetASPID();

private:
    long counter;
    bool utilitiesCalculated;
    std::map<std::string, conceptnet::Concept*> concepts;
    std::map<std::string, conceptnet::Edge*> edges;
    std::map<conceptnet::Concept*, double> utilities;

    static void generateEdge(Agraph_t* g, std::vector<conceptnet::Concept*>& openNodes, const std::string& term, const conceptnet::Edge* edge);
};
} // namespace cnoe