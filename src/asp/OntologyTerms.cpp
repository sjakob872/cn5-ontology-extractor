#include "asp/OntologyTerms.h"

namespace asp
{
const std::string OntologyTerms::CONCEPTNET_PREFIX = "cs_";
const std::string OntologyTerms::SUB_SET_OF = "subSetOf";
const std::string OntologyTerms::SUB_SET_OF_INTERNAL = "subSetOfInternal";
const std::string OntologyTerms::CLASSIFIED_AS = "classifiedAs";
const std::string OntologyTerms::CLASSIFIED_AS_INTERNAL = "classifiedAsInternal";
const std::string OntologyTerms::PROGRAM_SECTION = "commonsenseOntology";
const std::string OntologyTerms::INPUT = "is";
const std::string OntologyTerms::FROM_CONCEPT = "FromConcept";
const std::string OntologyTerms::TO_CONCEPT = "ToConcept";
const std::string OntologyTerms::CONCEPT = "Concept";
const std::string OntologyTerms::PARENT_CONCEPT = "ParentConcept";
const std::string OntologyTerms::INTER_CONCEPT = "InterConcept";
const std::string OntologyTerms::WEIGHT_VAR = "Weight";
const std::string OntologyTerms::WEIGHT1_VAR = "Weight1";
const std::string OntologyTerms::WEIGHT2_VAR = "Weight2";
const std::string OntologyTerms::WEIGHT = "weight";
const std::string OntologyTerms::MAX_WEIGHT = "MaxWeight";
const std::string OntologyTerms::MAX_TIMESTEP = "MaxTimeStep";
const std::string OntologyTerms::TIMESTEP = "TimeStep";
const std::string OntologyTerms::CURRENT_WEIGHT = "currentWeight";
const std::string OntologyTerms::ID = "Id";
const std::string OntologyTerms::RELATION = "Relation";
const std::string OntologyTerms::PROPERTY = "Property";
const std::string OntologyTerms::CHILD_PROPERTY = "ChildProperty";
const std::string OntologyTerms::PARENT_PROPERTY = "ParentProperty";
const std::string OntologyTerms::HAS_PROPERTY = "hasProperty";
const std::string OntologyTerms::PROPERTY_INHERITED_FROM = "propertyInheritedFrom";
const std::string OntologyTerms::FACET = "Facet";
const std::string OntologyTerms::HAS_FACET = "hasFacet";
const std::string OntologyTerms::FACET_INHERITED_FROM = "facetInheritedFrom";
const std::string OntologyTerms::FACET_OF = "facetOf";
const std::string OntologyTerms::SUB_CLASS_OF = "subClassOf";
const std::string OntologyTerms::SUB_PROPERTY_OF = "subPropertyOf";
const std::string OntologyTerms::HAS_PROPERTY_VALUE = "hasPropertyValue";
const std::string OntologyTerms::INDIVIDUAL = "Individual";
const std::string OntologyTerms::VALUE = "Value";
const std::string OntologyTerms::HAS_VALUE = "hasValue";
const std::string OntologyTerms::HAS_Domain = "hasDomain";
const std::string OntologyTerms::DOMAIN = "Domain";
const std::string OntologyTerms::DOMAIN_OF = "domainOf";
const std::string OntologyTerms::DOMAIN_INHERITED_FROM = "domainInheritedFrom";

} // namespace asp