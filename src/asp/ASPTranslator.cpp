#include "asp/ASPTranslator.h"

#include "AnswerGraph.h"
#include "Ontology.h"
#include "asp/OntologyTerms.h"
#include "conceptnet/Concept.h"
#include "conceptnet/Edge.h"
#include "conceptnet/Relations.h"

#include <algorithm>
#include <vector>

namespace asp
{
ASPTranslator::ASPTranslator()
{
    this->lowerIsARelation = conceptnet::relations[conceptnet::Relation::IsA];
    this->lowerIsARelation[0] = std::tolower(this->lowerIsARelation[0]);
    this->lowerFormOfRelation = conceptnet::relations[conceptnet::Relation::FormOf];
    this->lowerFormOfRelation[0] = std::tolower(this->lowerFormOfRelation[0]);
    this->lowerSynonymRelation = conceptnet::relations[conceptnet::Relation::Synonym];
    this->lowerSynonymRelation[0] = std::tolower(this->lowerSynonymRelation[0]);
    this->lowerHasPropertyRelation = conceptnet::relations[conceptnet::Relation::HasProperty];
    this->lowerHasPropertyRelation[0] = std::tolower(this->lowerHasPropertyRelation[0]);
}

cnoe::Ontology* ASPTranslator::extractOntology(cnoe::AnswerGraph* answerGraph, bool createExternals)
{
    if (answerGraph->getEdges().empty()) {
        return nullptr;
    }
    auto ontology = new cnoe::Ontology();
    ASPTranslator::createOntology(answerGraph, ontology, createExternals, true);
    return ontology;
}

void ASPTranslator::extractOntology(cnoe::AnswerGraph* answerGraph, cnoe::Ontology* ontology, bool createExternals)
{
    if (answerGraph->getEdges().empty()) {
        return;
    }
    ASPTranslator::createOntology(answerGraph, ontology, createExternals, false);
}

std::string ASPTranslator::generateOntologyRules(bool addShow)
{
    std::string program;
    this->generateSupportOntologyRules(program);
    this->generateSubSetRules(program);
    this->generateClassifiedRules(program);
    this->generatePropertyRules(program);
    ASPTranslator::generateFacetRules(program);
    ASPTranslator::generateDomainRules(program);

    if (addShow) {
        ASPTranslator::generateShowStatements(program);
    }
    return program;
}

void ASPTranslator::createOntology(cnoe::AnswerGraph* answerGraph, cnoe::Ontology* ontology, bool createExternals, bool newOntology)
{
    ontology->ontology.clear();
    ontology->externals.clear();
    if (newOntology) {
        answerGraph->resetASPID();
    }

    std::string program;
    //    std::vector<conceptnet::Concept*> openNodes;
    //    std::vector<conceptnet::Concept*> closedNodes;
    std::vector<conceptnet::Edge*> translatedEdges;
    //
    //    if(answerGraph->root != nullptr) {
    //        openNodes.push_back(answerGraph->root);
    //    } else {
    //        openNodes.push_back(answerGraph)
    //    }
    //  while (!openNodes.empty()) {
    //    conceptnet::Concept* node = openNodes[0];
    // std::cout << "AnswerGraph:renderDot: " << node->term << " " << node << std::endl;
    //  openNodes.erase(openNodes.begin());
    // if (std::find(closedNodes.begin(), closedNodes.end(), node) != closedNodes.end()) {
    //     continue;
    // }
    // closedNodes.push_back(node);

    for (auto node : answerGraph->getConcepts()) {
        for (conceptnet::Edge* edge : node.second->getEdges()) {
            if (std::find(translatedEdges.begin(), translatedEdges.end(), edge) != translatedEdges.end()) {
                continue;
            }
            if (newOntology) {
                edge->aspId = answerGraph->getNextASPID();
            }
            translatedEdges.push_back(edge);
            std::string tmp = "";
            std::string lowerRelation = conceptnet::relations[edge->relation];
            lowerRelation[0] = std::tolower(lowerRelation[0]);
            tmp.append(OntologyTerms::CONCEPTNET_PREFIX).append(lowerRelation);
            tmp.append("(\"")
                    .append(conceptToASPPredicate(edge->fromConcept->term))
                    .append("\", \"")
                    .append(conceptToASPPredicate(edge->toConcept->term))
                    .append("\", ")
                    .append(std::to_string(edge->aspId))
                    .append(")");
            std::string weightRule = "weight(" + std::to_string(edge->aspId) + ", " +
                                     std::to_string((int) ((edge->weight + edge->level + edge->relatedness) * 100.0)) + ", 0) :- " + tmp +
                                     ".\n";
            if (createExternals) {
                ontology->externals.push_back(tmp);
                tmp = "#external " + tmp;
            }
            tmp.append(".\n");
            tmp.append(weightRule);
            program.append(tmp);
            // openNodes.push_back(edge->toConcept);
            // openNodes.push_back(edge->fromConcept);
        }
    }
    ontology->ontology = program;
}

void ASPTranslator::generateShowStatements(std::string& program)
{
    program.append("#show subSetOf/4.\n");
    program.append("#show classifiedAs/3.\n");
    program.append("#show hasProperty/3.\n");
    program.append("#show propertyInheritedFrom/5.\n");
    program.append("#show facetOf/2.\n");
    program.append("#show typeOf/3.\n");
    program.append("#show valueRangeOf/3.\n");
    program.append("#show hasFacet/2.\n");
    program.append("#show facetInheritedFrom/3.\n");
    program.append("#show propertyViolation/4.\n");
    program.append("#show hasValue/5.\n");
    program.append("#show hasPropertyValue/3.\n");
    program.append("#show subPropertyOf/3.\n");
    program.append("#show domainOf/3.\n");
    program.append("#show hasDomain/3.\n");
    program.append("#show domainInheritedFrom/4.\n");
    program.append("#show is/2.\n");
}

void ASPTranslator::generateSupportOntologyRules(std::string& program)
{

    // currentWeight(Id, Weight, MaxTimeStep)
    // :- MaxTimeStep = #max{TimeStep : weight(Id, _, TimeStep)},
    // weight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                   OntologyTerms::MAX_TIMESTEP + ")")
            .append(" :- " + OntologyTerms::MAX_TIMESTEP + " = #max{" + OntologyTerms::TIMESTEP + " : " + OntologyTerms::WEIGHT + "(" +
                    OntologyTerms::ID + ", _, " + OntologyTerms::TIMESTEP + ")}, ")
            .append(OntologyTerms::WEIGHT + +"(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n\n");
    /**
     * isA
     */
    // isA(FromConcept, ToConcept, Weight) :- is(FromConcept, InterConcept), cs_isA(InterConcept, ToConcept, Id), currentWeight(Id, Weight,
    // MaxTimeStep).
    program.append(this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // isA(FromConcept, ToConcept, Weight) :- is(FromConcept, ToConcept), cs_isA(ToConcept, InterConcept, Id), isA(FromConcept,
    // InterConcept, Weight).
    program.append(this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // isA(FromConcept, ToConcept, Weight2) :- isA(FromConcept, InterConcept, Weight1), cs_isA(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerIsARelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    // isA(FromConcept, ToConcept, Weight2) :- formOf(FromConcept, InterConcept, Weight1), cs_isA(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerIsARelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    // isA(FromConcept, ToConcept, Weight2) :- synonym(FromConcept, InterConcept, Weight1),
    // cs_isA(InterConcept, ToConcept, Id), currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerIsARelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n\n");

    /**
     * formOf
     */
    // formOf(FromConcept, ToConcept, Weight) :- is(FromConcept, InterConcept), cs_formOf(InterConcept, ToConcept, Id), currentWeight(Id,
    // Weight, MaxTimeStep).
    program.append(this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // formOf(FromConcept, ToConcept, Weight) :- is(FromConcept, ToConcept), cs_formOf(ToConcept, InterConcept, Id), formOf(FromConcept,
    // InterConcept, Weight).
    program.append(this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // formOf(FromConcept, ToConcept, Weight2) :- formOf(FromConcept, InterConcept, Weight1), cs_formOf(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerFormOfRelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    // formOf(FromConcept, ToConcept, Weight2) :- isA(FromConcept, InterConcept, Weight1), cs_formOf(InterConcept, ToConcept,
    // Id), currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerFormOfRelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    // formOf(FromConcept, ToConcept, Weight2) :- synonym(FromConcept, InterConcept, Weight1),
    // cs_formOf(InterConcept, ToConcept, Id), currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerFormOfRelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n\n");

    /**
     * synonym
     */
    // synonym(FromConcept, ToConcept, Weight) :- is(FromConcept, InterConcept), cs_synonym(InterConcept, ToConcept, Id), currentWeight(Id,
    // Weight, MaxTimeStep).
    program.append(this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // synonym(FromConcept, ToConcept, Weight) :- is(FromConcept, ToConcept), cs_synonym(ToConcept, InterConcept, Id), synonym(FromConcept,
    // InterConcept, Weight).
    program.append(this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // synonym(FromConcept, ToConcept, Weight2) :- isA(FromConcept, InterConcept, Weight1), cs_synonym(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerSynonymRelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    //%* was cs_synonym(ToConcept, InterConcept, Id) before *%
    // synonym(ToConcept, FromConcept, Weight2) :- isA(FromConcept, InterConcept, Weight1), cs_synonym(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerSynonymRelation + +"(" + OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    // synonym(FromConcept, ToConcept, Weight2) :- formOf(FromConcept, InterConcept, Weight1), cs_synonym(InterConcept,
    // ToConcept, Id), currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerSynonymRelation + +"(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n");

    //%* was cs_synonym(ToConcept, InterConcept, Id) before *%
    // synonym(ToConcept, FromConcept, Weight2) :- formOf(FromConcept, InterConcept, Weight1), cs_synonym(InterConcept, ToConcept, Id),
    // currentWeight(Id, Weight2, MaxTimeStep), Weight1 < Weight2.
    program.append(this->lowerSynonymRelation + +"(" + OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", " +
                   OntologyTerms::WEIGHT2_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::WEIGHT1_VAR + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + +"(" + OntologyTerms::INTER_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT2_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + "), ")
            .append(OntologyTerms::WEIGHT1_VAR + " < " + OntologyTerms::WEIGHT2_VAR + ".\n\n");
}

void ASPTranslator::generateSubSetRules(std::string& program)
{
    // subSetOfInternal(FromConcept, ToConcept, is, 1) :- is(FromConcept, ToConcept).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::INPUT + ", 1)")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ").\n\n");

    // subSetOfInternal(FromConcept, ToConcept, formOf, Weight) :- formOf(InterConcept, ToConcept, _), formOf(InterConcept, FromConcept, _),
    // cs_formOf(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerFormOfRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, isA, Weight) :- formOf(InterConcept, ToConcept, _), isA(InterConcept, FromConcept, _),
    // cs_isA(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerIsARelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, formOf, Weight) :- formOf(InterConcept, ToConcept, _), isA(InterConcept, FromConcept, _),
    // cs_formOf(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerFormOfRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- formOf(InterConcept, ToConcept, _), synonym(InterConcept, FromConcept,
    // _), cs_synonym(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, formOf, Weight) :- formOf(InterConcept, ToConcept, _), synonym(InterConcept, FromConcept,
    // _), cs_formOf(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerFormOfRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n\n");

    // subSetOfInternal(FromConcept, ToConcept, isA, Weight) :- isA(InterConcept, ToConcept, _), isA(InterConcept, FromConcept, _),
    // cs_isA(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerIsARelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, formOf, Weight) :- isA(InterConcept, ToConcept, _), formOf(InterConcept, FromConcept, _),
    // cs_formOf(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerFormOfRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, isA, Weight) :- isA(InterConcept, ToConcept, _), formOf(InterConcept, FromConcept, _),
    // cs_isA(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerIsARelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerFormOfRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- isA(InterConcept, ToConcept, _), synonym(InterConcept, FromConcept, _),
    // cs_synonym(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, isA, Weight) :- isA(InterConcept, ToConcept, _), synonym(InterConcept, FromConcept, _),
    // cs_isA(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerIsARelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerIsARelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::FROM_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- synonym(InterConcept, ToConcept, _), cs_isA(ToConcept, _, Id),
    // cs_synonym(FromConcept, ToConcept, _), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerIsARelation + "(" + OntologyTerms::TO_CONCEPT + ", _, " +
                    OntologyTerms::ID + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- synonym(InterConcept, ToConcept, _), cs_formOf(ToConcept, _, Id),
    // cs_synonym(FromConcept, ToConcept, _), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerFormOfRelation + "(" + OntologyTerms::TO_CONCEPT + ", _, " +
                    OntologyTerms::ID + "), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- synonym(InterConcept, ToConcept, _),
    // cs_synonym(FromConcept, ToConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::INTER_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " +
                    OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // subSetOfInternal(FromConcept, ToConcept, synonym, Weight) :- synonym(FromConcept, InterConcept, _),
    // cs_synonym(ToConcept, FromConcept, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   this->lowerSynonymRelation + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::INTER_CONCEPT +
                    ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerSynonymRelation + "(" + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n\n");

    // subSetOf(FromConcept, ToConcept, Relation, MaxWeight) :- MaxWeight = #max{ Weight : subSetOfInternal(FromConcept, ToConcept,
    // Relation, Weight)}, subSetOfInternal(FromConcept, ToConcept, Relation, _).
    program.append(OntologyTerms::SUB_SET_OF + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::RELATION + ", " + OntologyTerms::MAX_WEIGHT + ")")
            .append(" :- " + OntologyTerms::MAX_WEIGHT + " = #max{ " + OntologyTerms::WEIGHT_VAR + " : " +
                    OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::RELATION + ", " + OntologyTerms::WEIGHT_VAR + ")}, ")
            .append(OntologyTerms::SUB_SET_OF_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::RELATION + ", _).\n\n");
}

void ASPTranslator::generateClassifiedRules(std::string& program)
{
    // classifiedAsInternal(FromConcept, ToConcept, 1) :- is(FromConcept, ToConcept).
    program.append(OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", 1)")
            .append(" :- " + OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ").\n");

    // classifiedAsInternal(FromConcept, ToConcept, MaxWeight) :- MaxWeight = #max{ Weight : synonym(FromConcept, ToConcept, Weight)},
    // synonym(FromConcept, ToConcept, _), is(FromConcept, _).
    program.append(OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::MAX_WEIGHT + ")")
            .append(" :- " + OntologyTerms::MAX_WEIGHT + " = #max{ " + OntologyTerms::WEIGHT_VAR + " : " + this->lowerSynonymRelation +
                    "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::WEIGHT_VAR + ")}, ")
            .append(this->lowerSynonymRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", _).\n");

    // classifiedAsInternal(FromConcept, ToConcept, MaxWeight) :- MaxWeight = #max{ Weight : isA(FromConcept, ToConcept, Weight)},
    // isA(FromConcept, ToConcept,
    // _), is(FromConcept, _).
    program.append(OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::MAX_WEIGHT + ")")
            .append(" :- " + OntologyTerms::MAX_WEIGHT + " = #max{ " + OntologyTerms::WEIGHT_VAR + " : " + this->lowerIsARelation + "(" +
                    OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::WEIGHT_VAR + ")}, ")
            .append(this->lowerIsARelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", _).\n");

    // classifiedAsInternal(FromConcept, ToConcept, MaxWeight) :- MaxWeight = #max{ Weight : formOf(FromConcept, ToConcept, Weight)},
    // formOf(FromConcept, ToConcept, _), is(FromConcept, _).
    program.append(OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::MAX_WEIGHT + ")")
            .append(" :- " + OntologyTerms::MAX_WEIGHT + " = #max{ " + OntologyTerms::WEIGHT_VAR + " : " + this->lowerFormOfRelation + "(" +
                    OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " + OntologyTerms::WEIGHT_VAR + ")}, ")
            .append(this->lowerFormOfRelation + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::INPUT + "(" + OntologyTerms::FROM_CONCEPT + ", _).\n\n");

    // classifiedAs(FromConcept, ToConcept, MaxWeight) :- MaxWeight = #max{ Weight : classifiedAsInternal(FromConcept, ToConcept, Weight)},
    // classifiedAsInternal(FromConcept, ToConcept, _).
    program.append(OntologyTerms::CLASSIFIED_AS + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                   OntologyTerms::MAX_WEIGHT + ")")
            .append(" :- " + OntologyTerms::MAX_WEIGHT + " = #max{ " + OntologyTerms::WEIGHT_VAR + " : " +
                    OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::WEIGHT_VAR + ")}, ")
            .append(OntologyTerms::CLASSIFIED_AS_INTERNAL + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT +
                    ", _).\n\n");
}

std::string ASPTranslator::conceptToASPPredicate(std::string conceptName)
{
    std::replace(conceptName.begin(), conceptName.end(), '.', '_');
    std::replace(conceptName.begin(), conceptName.end(), ',', '_');
    std::replace(conceptName.begin(), conceptName.end(), ' ', '_');
    return conceptName;
}

void ASPTranslator::generatePropertyRules(std::string& program)
{
    // hasProperty(FromConcept, Property, Weight) :- classifiedAs(FromConcept, ToConcept, _), cs_hasProperty(ToConcept, Property, Id),
    // currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::PROPERTY + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::CLASSIFIED_AS + "(" + OntologyTerms::FROM_CONCEPT + ", " + OntologyTerms::TO_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerHasPropertyRelation + "(" + OntologyTerms::TO_CONCEPT + ", " +
                    OntologyTerms::PROPERTY + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n");

    // propertyInheritedFrom(Concept, Property, ParentConcept, subClassOf, Weight) :- classifiedAs(Concept, ParentConcept, _),
    // cs_hasProperty(ParentConcept, Property, Id), currentWeight(Id, Weight, MaxTimeStep).
    program.append(OntologyTerms::PROPERTY_INHERITED_FROM + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PROPERTY + ", " +
                   OntologyTerms::PARENT_CONCEPT + ", " + OntologyTerms::SUB_CLASS_OF + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::CLASSIFIED_AS + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PARENT_CONCEPT + ", _), ")
            .append(OntologyTerms::CONCEPTNET_PREFIX + this->lowerHasPropertyRelation + "(" + OntologyTerms::PARENT_CONCEPT + ", " +
                    OntologyTerms::PROPERTY + ", " + OntologyTerms::ID + "), ")
            .append(OntologyTerms::CURRENT_WEIGHT + "(" + OntologyTerms::ID + ", " + OntologyTerms::WEIGHT_VAR + ", " +
                    OntologyTerms::MAX_TIMESTEP + ").\n\n");

    // hasProperty(Concept, ChildProperty, Weight) :- subPropertyOf(ChildProperty, ParentProperty, TimeStep), hasProperty(Concept,
    // ParentProperty, Weight).
    program.append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::CHILD_PROPERTY + ", " +
                   OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::SUB_PROPERTY_OF + "(" + OntologyTerms::CHILD_PROPERTY + ", " + OntologyTerms::PARENT_PROPERTY +
                    ", " + OntologyTerms::TIMESTEP + "), ")
            .append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PARENT_PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // propertyInheritedFrom(Concept, ChildProperty, ParentProperty, subPropertyOf, Weight) :- subPropertyOf(ChildProperty, ParentProperty,
    // TimeStep), hasProperty(Concept, ChildProperty, Weight), hasProperty(Concept, ParentProperty, Weight).
    program.append(OntologyTerms::PROPERTY_INHERITED_FROM + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::CHILD_PROPERTY + ", " +
                   OntologyTerms::PARENT_PROPERTY + ", " + OntologyTerms::SUB_PROPERTY_OF + ", " + OntologyTerms::WEIGHT_VAR + ")")
            .append(" :- " + OntologyTerms::SUB_PROPERTY_OF + "(" + OntologyTerms::CHILD_PROPERTY + ", " + OntologyTerms::PARENT_PROPERTY +
                    ", " + OntologyTerms::TIMESTEP + "), ")
            .append(this->lowerHasPropertyRelation + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::CHILD_PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + "), ")
            .append(this->lowerHasPropertyRelation + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PARENT_PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n\n");
}

void ASPTranslator::generateFacetRules(std::string& program)
{
    // hasFacet(Concept, Facet) :- facetOf(Facet, Property), hasProperty(Concept, Property, Weight).
    program.append(OntologyTerms::HAS_FACET + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::FACET + ")")
            .append(" :- " + OntologyTerms::FACET_OF + "(" + OntologyTerms::FACET + ", " + OntologyTerms::PROPERTY + "), ")
            .append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // facetInheritedFrom(Facet, Concept, ParentConcept) :- facetOf(Facet, Property), propertyInheritedFrom(Concept, Property,
    // ParentConcept, Weight).
    program.append(OntologyTerms::FACET_INHERITED_FROM + "(" + OntologyTerms::FACET + ", " + OntologyTerms::CONCEPT + ", " +
                   OntologyTerms::PARENT_CONCEPT + ")")
            .append(" :- " + OntologyTerms::FACET_OF + "(" + OntologyTerms::FACET + ", " + OntologyTerms::PROPERTY + "), ")
            .append(OntologyTerms::PROPERTY_INHERITED_FROM + "(" + OntologyTerms::CONCEPT + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::PARENT_CONCEPT + ", " + OntologyTerms::WEIGHT_VAR + ").\n\n");

    // hasPropertyValue(Individual, Property, Value) :- hasProperty(Individual, Property, _), facetOf(Facet, Property),
    // hasValue(Individual,Facet,Property,Value,TimeStep).
    program.append(OntologyTerms::HAS_PROPERTY_VALUE + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::PROPERTY + ", " +
                   OntologyTerms::VALUE + ")")
            .append(" :- " + OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::PROPERTY + ", _), ")
            .append(OntologyTerms::FACET_OF + "(" + OntologyTerms::FACET + ", " + OntologyTerms::PROPERTY + "), ")
            .append(OntologyTerms::HAS_VALUE + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::FACET + ", " +
                    OntologyTerms::PROPERTY + ", " + OntologyTerms::VALUE + ", " + OntologyTerms::TIMESTEP + ").\n\n");
}

void ASPTranslator::generateDomainRules(std::string& program)
{
    // hasDomain(Individual, Domain, TimeStep) :- domainOf(Domain, Property, TimeStep), hasProperty(Individual, Property, Weight).
    program.append(OntologyTerms::HAS_Domain + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::DOMAIN + ", " +
                   OntologyTerms::TIMESTEP + ")")
            .append(" :- " + OntologyTerms::DOMAIN_OF + "(" + OntologyTerms::DOMAIN + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::TIMESTEP + "), ")
            .append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n");

    // domainInheritedFrom(Individual, Domain, Property, TimeStep) :- domainOf(Domain, Property, TimeStep), hasProperty(Individual,
    // Property, Weight).
    program.append(OntologyTerms::DOMAIN_INHERITED_FROM + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::DOMAIN + ", " +
                   OntologyTerms::PROPERTY + ", " + OntologyTerms::TIMESTEP + ")")
            .append(" :- " + OntologyTerms::DOMAIN_OF + "(" + OntologyTerms::DOMAIN + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::TIMESTEP + "), ")
            .append(OntologyTerms::HAS_PROPERTY + "(" + OntologyTerms::INDIVIDUAL + ", " + OntologyTerms::PROPERTY + ", " +
                    OntologyTerms::WEIGHT_VAR + ").\n\n");
}

void ASPTranslator::translateEdge(cnoe::AnswerGraph* answerGraph, cnoe::Ontology* ontology, conceptnet::Edge* edge, bool createExternals)
{
    std::string tmp = "";
    std::string lowerRelation = conceptnet::relations[edge->relation];
    lowerRelation[0] = std::tolower(lowerRelation[0]);
    tmp.append(OntologyTerms::CONCEPTNET_PREFIX).append(lowerRelation);
    tmp.append("(\"")
            .append(conceptToASPPredicate(edge->fromConcept->term))
            .append("\", \"")
            .append(conceptToASPPredicate(edge->toConcept->term))
            .append("\", ")
            .append(std::to_string(edge->aspId))
            .append(")");
    std::string weightRule = "weight(" + std::to_string(edge->aspId) + ", " +
                             std::to_string((int) ((edge->weight + edge->level + edge->relatedness) * 100.0)) + ", 0) :- " + tmp + ".\n";
    if (createExternals) {
        ontology->externals.push_back(tmp);
        tmp = "#external " + tmp;
    }
    tmp.append(".\n");
    tmp.append(weightRule);
    ontology->ontology.append(tmp);
}

void ASPTranslator::translateEdges(cnoe::AnswerGraph* answerGraph, cnoe::Ontology* ontology, bool createExternals)
{

    ontology->ontology.clear();
    ontology->externals.clear();
    std::vector<conceptnet::Edge*> translatedEdges;

    for (auto edge : answerGraph->getEdges()) {
        if (std::find(translatedEdges.begin(), translatedEdges.end(), edge.second) != translatedEdges.end()) {
            continue;
        }
        translatedEdges.push_back(edge.second);
        std::string tmp = "";
        std::string lowerRelation = conceptnet::relations[edge.second->relation];
        lowerRelation[0] = std::tolower(lowerRelation[0]);
        tmp.append(OntologyTerms::CONCEPTNET_PREFIX).append(lowerRelation);
        tmp.append("(\"")
                .append(conceptToASPPredicate(edge.second->fromConcept->term))
                .append("\", \"")
                .append(conceptToASPPredicate(edge.second->toConcept->term))
                .append("\", ")
                .append(std::to_string(edge.second->aspId))
                .append(")");
        std::string weightRule = "weight(" + std::to_string(edge.second->aspId) + ", " +
                                 std::to_string((int) ((edge.second->weight + edge.second->level + edge.second->relatedness) * 100.0)) +
                                 ", 0) :- " + tmp + ".\n";
        if (createExternals) {
            ontology->externals.push_back(tmp);
            tmp = "#external " + tmp;
        }
        tmp.append(".\n");
        tmp.append(weightRule);
        ontology->ontology.append(tmp);
    }
}

} // namespace asp