#include "OntologyExtractor.h"
#include "conceptnet/Edge.h"
#include "conceptnet/Concept.h"
#include "AnswerGraph.h"
#include "Ontology.h"

#include <iostream>
#include <chrono>
#include <cstdlib>
#include <gvc.h>
#include <reasoner/asp/Solver.h>

//#define ONTOLOGY_DEBUG
int main(int argc, char** argv)
{
    std::string rootConcept = "thing";
    if (argc < 2) {
        std::cout << "Main: No root concept provided using 'thing'." << std::endl;
    } else {
        rootConcept = argv[1];
    }

    auto ontologyExtractor = new cnoe::OntologyExtractor();
    ontologyExtractor->extract(rootConcept);

    std::string domainConfigFolder = ::getenv("DOMAIN_CONFIG_FOLDER");
    std::string facts = cnoe::OntologyExtractor::readFromFile(domainConfigFolder + "/ontologies/test/test_facts.lp");

    /**
         * ################# ADD #################
         */

    auto start = std::chrono::high_resolution_clock::now();
    auto solver = new reasoner::asp::Solver({});

    solver->add("", {}, ontologyExtractor->getOntology()->ontology.c_str());

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Main: Adding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Ground #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{"", {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Main: Grounding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Assign Externals #################
     */

    start = std::chrono::high_resolution_clock::now();
    for (const auto& external : ontologyExtractor->getOntology()->externals) {
        solver->assignExternal(solver->parseValue(external), Clingo::TruthValue::True);
    }
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Main: External runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Solve #################
     */
    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Main: Solving runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Query #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->add("test", {}, (ontologyExtractor->getOntology()->rules + facts).c_str());
    solver->ground({{"test", {}}}, nullptr);
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Main: Query runtime: " << duration / 1000.0 << " ms" << std::endl;

    auto models = solver->getCurrentModels();
    std::stringstream ss;
    Agraph_t* g;
    static GVC_t* gvc;
    if (!gvc) {
        gvc = gvContext();
    }
    /* Create a simple digraph */
    g = agopen(const_cast<char*>("cn5_Ontology"), Agdirected, nullptr);
    agsafeset(g, const_cast<char*>("rankdir"), const_cast<char*>("LR"), const_cast<char*>(""));
    ss << "Solution: ";
    for (size_t i = 0; i < models.size(); i++) {
        ss << "Model number " << i + 1 << ":\n" << std::endl;
        for (auto atom : models.at(i)) {
            ss << atom << " ";
            cnoe::OntologyExtractor::generateOntologyGraph(g, atom.to_string());
        }
        ss << std::endl;
    }
    std::cout << ss.str() << std::endl;
    gvLayout(gvc, g, "dot");
    /* Output in .dot format */
    FILE* fptr;
    std::string dot = domainConfigFolder + "/ontologies/test/classification.dot";
    fptr = fopen(dot.c_str(), "w");
    gvRender(gvc, g, "dot", fptr);
    fclose(fptr);
    gvFreeLayout(gvc, g);
    agclose(g);

    std::string pdf = domainConfigFolder + "/ontologies/test/classification.pdf";
    std::system(std::string("dot -Tpdf " + dot + " -o " + pdf).c_str());

    delete ontologyExtractor;
    return EXIT_SUCCESS;
}