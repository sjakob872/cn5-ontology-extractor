#include "conceptnet/ConceptPath.h"

#include "conceptnet/Edge.h"
#include "conceptnet/Concept.h"

#include <iostream>
#include <sstream>

namespace conceptnet
{
ConceptPath::ConceptPath(Concept* end)
        : utility(1)
{
    this->end = end;
}

ConceptPath::ConceptPath(const ConceptPath& other)
{
    this->utility = other.utility;
    for (Edge* edge : other.getPath()) {
        this->addToPath(edge);
    }
    this->end = other.getEnd();
}

void ConceptPath::addToPath(Edge* edge)
{
    this->path.push_back(edge);
    if (edge->toConcept == this->end) {
        this->end = edge->fromConcept;
    } else {
        this->end = edge->toConcept;
    }
}

void ConceptPath::calculateUtility()
{
    if (this->path.empty()) {
        this->utility = 0;
        return;
    }

    for (auto& i : this->path) {
        this->utility += getEdgeWeight(i);
    }
}

bool ConceptPath::containsNode(Concept* c)
{
    for (Edge* edge : this->path) {
        //        std::cout << "ConceptPath: Node " << concept->term << "\n Edge: " << edge->toString() << std::endl;
        if (edge->fromConcept == c || edge->toConcept == c) {
            return true;
        }
    }
    return false;
}

Concept* ConceptPath::getEnd() const
{
    return this->end;
}

std::vector<Edge*> ConceptPath::getPath() const
{
    return this->path;
}

std::string ConceptPath::toString(const std::string& indent) const
{
    std::stringstream ss;
    ss << indent << "#ConceptPath: End: " << this->end->term << " Length: " << this->path.size() << std::endl;
    for (Edge* edge : path) {
        ss << edge->toString(indent) << std::endl;
    }
    return ss.str();
}

double ConceptPath::getUtility() const
{
    return this->utility;
}

double ConceptPath::getEdgeWeight(Edge* edge)
{
    if (edge->relation == conceptnet::Synonym || edge->relation == conceptnet::SimilarTo || edge->relation == conceptnet::InstanceOf) {
        return 0;
    } else {
        return edge->weight;
    }
}

} // namespace conceptnet
