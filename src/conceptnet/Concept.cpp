#include "conceptnet/Concept.h"

#include "conceptnet/Edge.h"

#include <algorithm>

namespace conceptnet
{
Concept::Concept(std::string id, std::string term, std::string senseLabel)
        : id(id)
        , term(term)
        , senseLabel(senseLabel)
{
}

void Concept::addEdge(Edge* edge)
{
    if (std::find(this->edges.begin(), this->edges.end(), edge) != this->edges.end()) {
        return;
    }
    this->edges.push_back(edge);
    edge->getOpposite(this)->addEdge(edge);
}

void Concept::addEdges(const std::vector<Edge*>& edgesToAdd)
{
    for (Edge* edge : edgesToAdd) {
        this->addEdge(edge);
    }
}

std::vector<Edge*> Concept::getEdges()
{
    return this->edges;
}

std::vector<Concept*> Concept::getConnectedConcepts(Relation relation, bool includeEquivalents)
{
    std::vector<conceptnet::Relation> rels;
    rels.push_back(relation);
    return this->getConnectedConcepts(rels, includeEquivalents);
}

std::vector<Concept*> Concept::getConnectedConcepts(const std::vector<Relation>& rels, bool includeEquivalents)
{
    std::vector<Concept*> resultConcepts;
    for (Edge* edge : this->edges) {
        for (conceptnet::Relation relation : rels) {
            if (edge->relation == relation) {
                resultConcepts.push_back(edge->getOpposite(this));
            }
        }
    }

    if (!includeEquivalents) {
        return resultConcepts;
    }

    std::vector<Concept*> equivalentConcepts;
    for (Concept* concept : resultConcepts) {
        for (Edge* edge : concept->edges) {
            if (edge->relation == Relation::Synonym || edge->relation == Relation::InstanceOf || edge->relation == Relation::SimilarTo) {
                equivalentConcepts.push_back(edge->getOpposite(concept));
            }
        }
    }

    resultConcepts.insert(resultConcepts.end(), equivalentConcepts.begin(), equivalentConcepts.end());
    return resultConcepts;
}

void Concept::removeEdge(Edge* edge) {
    auto iter = std::find(this->edges.begin(), this->edges.end(), edge);
    if(iter == this->edges.end()) {
        return;
    }
    this->edges.erase(iter);
    edge->getOpposite(this)->removeEdge(edge);
}

} // namespace conceptnet

bool operator==(const conceptnet::Concept& one, const conceptnet::Concept& another)
{
    return one.term == another.term;
}
