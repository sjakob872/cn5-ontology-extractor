#include "conceptnet/Edge.h"

#include "conceptnet/Concept.h"
#include "conceptnet/Relations.h"

#include <iostream>
#include <sstream>

namespace conceptnet
{

Edge::Edge(std::string id, std::string language, Concept* fromConcept, Concept* toConcept, Relation relation, double weight)
{
    this->id = id;
    this->language = language;
    this->level = 0.0;
    this->relatedness = 0.0;
    this->weight = weight;
    this->relation = relation;
    this->fromConcept = fromConcept;
    this->toConcept = toConcept;
    this->causesInconsistency = false;
    this->timeStamp = 0;
    this->aspId = -1;
}

bool Edge::operator<(const Edge& another) const
{
    return this->weight > another.weight;
}

std::string Edge::toString(const std::string& indent) const
{
    std::stringstream ss;
    ss << indent << "Edge with Language: " << this->language << " From Concept: " << this->fromConcept->term << " Sense: " << this->fromConcept->senseLabel
       << " Relation: " << relations[this->relation] << " to Concept: " << this->toConcept->term << " Sense: " << this->toConcept->senseLabel
       << " Weight: " << this->weight << " Causes Inconsistency: " << (this->causesInconsistency ? "\033[1;31mtrue\033[0m" : "false");
    return ss.str();
}

Concept* Edge::getOpposite(Concept* c) const
{
    if (this->fromConcept == c) {
        return this->toConcept;
    } else if (this->toConcept == c) {
        return this->fromConcept;
    } else {
        std::cout << "Edge::getOpposite returned null" << this->fromConcept->term << " " << this->toConcept->term << " " << c->term << std::endl;
        return nullptr;
    }
}
} // namespace conceptnet

bool operator==(const conceptnet::Edge& one, const conceptnet::Edge& another)
{
    return one.fromConcept->term == another.fromConcept->term && one.toConcept->term == another.toConcept->term && one.relation == another.relation;
}
