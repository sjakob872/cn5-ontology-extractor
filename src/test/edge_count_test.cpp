#include <string>
#include <vector>
#include <fstream>
#include <yaml-cpp/yaml.h>
#include <curl/curl.h>
#include "conceptnet/Relations.h"

std::size_t callback(const char* in, std::size_t size, std::size_t num, std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}

std::string httpGet(const std::string& url)
{
    CURL* curl = curl_easy_init();

    // Set remote URL.
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    // Don't bother trying IPv6, which would increase DNS resolution time.
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

    // Don't wait forever, time out after 10 seconds.
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

    // Follow HTTP redirects if necessary.
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    // Response information.
    int httpCode(0);
    std::string* tmp = new std::string();
    std::unique_ptr<std::string> httpData(move(tmp));

    // Hook up data handling function.
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

    // Hook up data container (will be passed as the last parameter to the
    // callback handling function).  Can be any pointer type, since it will
    // internally be passed as a void pointer.
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

    // Run our HTTP GET command, capture the HTTP response code, and clean up.
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    curl_easy_cleanup(curl);

    return *httpData;
}

std::string countEdges(const std::string& json, int& counter, std::ofstream& file)
{
    YAML::Node node;
    node = YAML::Load(json);
    if (!node && YAML::NodeType::Null != node.Type()) {
        return "";
    }
    //bool written = false;
    YAML::Node jsonEdges = node["edges"];
    for (size_t i = 0; i < jsonEdges.size(); i++) {
        YAML::Node edge = jsonEdges[i];
        //double weight = edge["weight"].as<double>();
        // end of edge
        YAML::Node end = edge["end"];
        if (!end["language"].IsDefined()) {
            continue;
        }
        std::string endLanguage = end["language"].as<std::string>();
        // skip non English
        if (endLanguage != "en") {
            continue;
        }
        std::string endTerm = end["term"].as<std::string>();
        /*if (this->conceptContainsForbiddenCharacter(endTerm)) {
            continue;
        }*/
        std::string endSenseLabel = "";
        if (end["sense_label"].IsDefined()) {
            endSenseLabel = end["sense_label"].as<std::string>();
        }
        std::string endID = end["@id"].as<std::string>();
        // start of edge
        YAML::Node start = edge["start"];
        if (!start["language"].IsDefined()) {
            continue;
        }
        std::string startLanguage = start["language"].as<std::string>();
        // skip non English
        if (startLanguage != "en") {
            continue;
        }
        counter++;
    }
    if (!node["view"].IsDefined() && !node["view"]["nextPage"].IsDefined()) {
        return "";
    }
    if (node["view"].IsDefined() && !node["view"]["nextPage"].IsDefined()) {
        return "";
    }
    return node["view"]["nextPage"].as<std::string>();
}

int main(int argc, char** argv) {
    std::ofstream file;
    file.open("/home/stefan/Desktop/results.txt");

    for(int i = 0; i < 40; i++) {
        int counter = 0;
        file << conceptnet::relations[i] << "\n";
        std::string json = httpGet("http://api.localhost:8084/query?rel=/r/" + std::string(conceptnet::relations[i]) + "&limit=1000");
        YAML::Node node;
        node = YAML::Load(json);
        if (!node && YAML::NodeType::Null != node.Type()) {
            continue;
        }
        std::string nextPage = countEdges(json, counter, file);
        while (!nextPage.empty()) {
            json = httpGet("http://api.localhost:8084" + nextPage);
            nextPage = countEdges(json, counter, file);
        }
        file << "Edges: " << counter << "\n\n" << std::flush;
        counter = 0;
    }


    file.close();
    return 0;
}