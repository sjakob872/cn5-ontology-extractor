#include "OntologyExtractor.h"

#include <reasoner/asp/Solver.h>

#include <cgraph.h>
#include <chrono>
#include <fstream>
#include <gvc.h>
#include <gvcext.h>
#include <string>

/**
 * clingo version 5.3.1
Reading from setconf-py.lp
tester[0..0] - Tester Options
  tester[0..0].solver[0..0] - Solver Options
    tester[0..0].solver[0..0].opt_strategy[=None] - Configure optimization strategy
      %A: {bb|usc}[,<tactics>]
        bb : Model-guided optimization with <tactics {lin|hier|inc|dec}> [lin]
          lin : Basic lexicographical descent
          hier: Hierarchical (highest priority criteria first) descent
          inc : Hierarchical descent with exponentially increasing steps
          dec : Hierarchical descent with exponentially decreasing steps
        usc: Core-guided optimization with <tactics>: <relax>[,<opts>]
          <relax>: Relaxation algorithm {oll|one|k|pmres}                [oll]
            oll    : Use strategy from unclasp
            one    : Add one cardinality constraint per core
            k[,<n>]: Add cardinality constraints of bounded size ([0]=dynamic)
            pmres  : Add clauses of size 3
          <opts> : Tactics <list {disjoint|succinct|stratify}>|<mask {0..7}>
            disjoint: Disjoint-core preprocessing                    (1)
            succinct: No redundant (symmetry) constraints            (2)
            stratify: Stratification heuristic for handling weights  (4)
    tester[0..0].solver[0..0].opt_usc_shrink[=None] - Enable core-shrinking in core-guided optimization
      %A: <algo>[,<limit> (0=no limit)]
        <algo> : Use algorithm {lin|inv|bin|rgs|exp|min}
          lin  : Forward linear search unsat
          inv  : Inverse linear search not unsat
          bin  : Binary search
          rgs  : Repeated geometric sequence until unsat
          exp  : Exponential search until unsat
          min  : Linear search for subset minimal core
        <limit>: Limit solve calls to 2^<n> conflicts [10]
    tester[0..0].solver[0..0].opt_heuristic[=None] - Use opt. in <list {sign|model}> heuristics
    tester[0..0].solver[0..0].restart_on_model[=None] - Restart after each model

    tester[0..0].solver[0..0].lookahead[=None] - Configure failed-literal detection (fld)
      %A: <type>[,<limit>] / Implicit: %I
        <type> : Run fld via {atom|body|hybrid} lookahead
        <limit>: Disable fld after <limit> applications ([0]=no limit)
      --lookahead=atom is default if --no-lookback is used

    tester[0..0].solver[0..0].heuristic[=None] - Configure decision heuristic
      %A: {Berkmin|Vmtf|Vsids|Domain|Unit|None}[,<n>]
        Berkmin: Use BerkMin-like heuristic (Check last <n> nogoods [0]=all)
        Vmtf   : Use Siege-like heuristic (Move <n> literals to the front [8])
        Vsids  : Use Chaff-like heuristic (Use 1.0/0.<n> as decay factor  [95])
        Domain : Use domain knowledge in Vsids-like heuristic
        Unit   : Use Smodels-like heuristic (Default if --no-lookback)
        None   : Select the first free variable
    tester[0..0].solver[0..0].init_moms[=None] - Initialize heuristic with MOMS-score
    tester[0..0].solver[0..0].score_res[=None] - Resolution score {auto|min|set|multiset}
    tester[0..0].solver[0..0].score_other[=None] - Score other learnt nogoods: {auto|no|loop|all}
    tester[0..0].solver[0..0].sign_def[=None] - Default sign: {asp|pos|neg|rnd}
    tester[0..0].solver[0..0].sign_fix[=None] - Disable sign heuristics and use default signs only
    tester[0..0].solver[0..0].berk_huang[=None] - Enable Huang-scoring in Berkmin
    tester[0..0].solver[0..0].vsids_acids[=None] - Enable acids-scheme in Vsids/Domain
    tester[0..0].solver[0..0].vsids_progress[=None] - Enable dynamic decaying scheme in Vsids/Domain
      %A: <n>[,<i {1..100}>][,<c>]|(0=disable)
        <n> : Set initial decay factor to 1.0/0.<n>
        <i> : Set decay update to <i>/100.0      [1]
        <c> : Decrease decay every <c> conflicts [5000]
    tester[0..0].solver[0..0].nant[=None] - Prefer negative antecedents of P in heuristic
    tester[0..0].solver[0..0].dom_mod[=None] - Default modification for domain heuristic
      %A: (no|<mod>[,<pick>])
        <mod>  : Modifier {level|pos|true|neg|false|init|factor}
        <pick> : Apply <mod> to (all | <list {scc|hcc|disj|opt|show}>) atoms
    tester[0..0].solver[0..0].save_progress[=None] - Use RSat-like progress saving on backjumps > %A
    tester[0..0].solver[0..0].init_watches[=None] - Watched literal initialization: {rnd|first|least}
    tester[0..0].solver[0..0].update_mode[=None] - Process messages on {propagate|conflict}
    tester[0..0].solver[0..0].acyc_prop[=None] - Use backward inference in acyc propagation
    tester[0..0].solver[0..0].seed[=None] - Set random number generator's seed to %A
    tester[0..0].solver[0..0].no_lookback[=None] - Disable all lookback strategies

    tester[0..0].solver[0..0].forget_on_step[=None] - Configure forgetting on (incremental) step
      %A: <list {varScores|signs|lemmaScores|lemmas}>|<mask {0..15}>

    tester[0..0].solver[0..0].strengthen[=None] - Use MiniSAT-like conflict nogood strengthening
      %A: <mode>[,<type>][,<bump {yes|no}>]
        <mode>: Use {local|recursive} self-subsumption check
        <type>: Follow {all|short|binary} antecedents [all]
        <bump>: Bump activities of antecedents        [yes]
    tester[0..0].solver[0..0].otfs[=None] - Enable {1=partial|2=full} on-the-fly subsumption
    tester[0..0].solver[0..0].update_lbd[=None] - Configure LBD updates during conflict resolution
      %A: <mode {less|glucose|pseudo}>[,<n {0..127}>]
        less   : update to X = new LBD   iff X   < previous LBD
        glucose: update to X = new LBD   iff X+1 < previous LBD
        pseudo : update to X = new LBD+1 iff X   < previous LBD
           <n> : Protect updated nogoods on next reduce if X <= <n>
    tester[0..0].solver[0..0].update_act[=None] - Enable LBD-based activity bumping
    tester[0..0].solver[0..0].reverse_arcs[=None] - Enable ManySAT-like inverse-arc learning
    tester[0..0].solver[0..0].contraction[=None] - Configure handling of long learnt nogoods
      %A: <n>[,<rep>]
        <n>  : Contract nogoods if size > <n> (0=disable)
        <rep>: Nogood replacement {no|decisionSeq|allUIP|dynamic} [no]

    tester[0..0].solver[0..0].loops[=None] - Configure learning of loop nogoods
      %A: {common|distinct|shared|no}
        common  : Create loop nogoods for atoms in an unfounded set
        distinct: Create distinct loop nogood for each atom in an unfounded set
        shared  : Create loop formula for a whole unfounded set
        no      : Do not learn loop formulas

    tester[0..0].solver[0..0].partial_check[=None] - Configure partial stability tests
      %A: <p>[,<h>] / Implicit: %I
        <p>: Partial check skip percentage
        <h>: Init/update value for high bound ([0]=umax)
    tester[0..0].solver[0..0].sign_def_disj[=None] - Default sign for atoms in disjunctions
    tester[0..0].solver[0..0].rand_freq[=None] - Make random decisions with probability %A
    tester[0..0].solver[0..0].rand_prob[=None] - Do <n> random searches with [<m>=100] conflicts
    tester[0..0].solver[0..0].restarts[=None] - Configure restart policy
      %A: <type {D|F|L|x|+}>,<n {1..umax}>[,<args>][,<lim>]
        F,<n>    : Run fixed sequence of <n> conflicts
        L,<n>    : Run Luby et al.'s sequence with unit length <n>
        x,<n>,<f>: Run geometric seq. of <n>*(<f>^i) conflicts  (<f> >= 1.0)
        +,<n>,<m>: Run arithmetic seq. of <n>+(<m>*i) conflicts (<m {0..umax}>)
        ...,<lim>: Repeat seq. every <lim>+j restarts           (<type> != F)
        D,<n>,<f>: Restart based on moving LBD average over last <n> conflicts
                   Mavg(<n>,LBD)*<f> > avg(LBD)
                   use conflict level average if <lim> > 0 and avg(LBD) > <lim>
      no|0       : Disable restarts
    tester[0..0].solver[0..0].reset_restarts[=None] - Update restart seq. on model {no|repeat|disable}
    tester[0..0].solver[0..0].local_restarts[=None] - Use Ryvchin et al.'s local restarts
    tester[0..0].solver[0..0].counter_restarts[=None] - Use counter implication restarts
      %A: (<rate>[,<bump>] | {0|no})
      <rate>: Interval in number of restarts
      <bump>: Bump factor applied to indegrees
    tester[0..0].solver[0..0].block_restarts[=None] - Use glucose-style blocking restarts
      %A: <n>[,<R {1.0..5.0}>][,<c>]
        <n>: Window size for moving average (0=disable blocking)
        <R>: Block restart if assignment > average * <R>  [1.4]
        <c>: Disable blocking for the first <c> conflicts [10000]

    tester[0..0].solver[0..0].shuffle[=None] - Shuffle problem after <n1>+(<n2>*i) restarts

    tester[0..0].solver[0..0].deletion[=None] - Configure deletion algorithm [%D]
      %A: <algo>[,<n {1..100}>][,<sc>]
        <algo>: Use {basic|sort|ipSort|ipHeap} algorithm
        <n>   : Delete at most <n>%% of nogoods on reduction    [75]
        <sc>  : Use {activity|lbd|mixed} nogood scores    [activity]
      no      : Disable nogood deletion
    tester[0..0].solver[0..0].del_grow[=None] - Configure size-based deletion policy
      %A: <f>[,<g>][,<sched>] (<f> >= 1.0)
        <f>     : Keep at most T = X*(<f>^i) learnt nogoods with X being the
                  initial limit and i the number of times <sched> fired
        <g>     : Stop growth once T > P*<g> (0=no limit)      [3.0]
        <sched> : Set grow schedule (<type {F|L|x|+}>) [grow on restart]
    tester[0..0].solver[0..0].del_cfl[=None] - Configure conflict-based deletion policy
      %A:   <type {F|L|x|+}>,<args>... (see restarts)
    tester[0..0].solver[0..0].del_init[=None] - Configure initial deletion limit
      %A: <f>[,<n>,<o>] (<f> > 0)
        <f>    : Set initial limit to P=estimated problem size/<f> [%D]
        <n>,<o>: Clamp initial limit to the range [<n>,<n>+<o>]
    tester[0..0].solver[0..0].del_estimate[=None] - Use estimated problem complexity in limits
    tester[0..0].solver[0..0].del_max[=None] - Keep at most <n> learnt nogoods taking up to <X> MB
    tester[0..0].solver[0..0].del_glue[=None] - Configure glue clause handling
      %A: <n {0..15}>[,<m {0|1}>]
        <n>: Do not delete nogoods with LBD <= <n>
        <m>: Count (0) or ignore (1) glue clauses in size limit [0]
    tester[0..0].solver[0..0].del_on_restart[=None] - Delete %A%% of learnt nogoods on each restart
  tester[0..0].configuration[=None] - Initializes this configuration
      <arg>: {auto|frumpy|jumpy|tweety|handy|crafty|trendy|many|<file>}
        auto  : Select configuration based on problem type
        frumpy: Use conservative defaults
        jumpy : Use aggressive defaults
        tweety: Use defaults geared towards asp problems
        handy : Use defaults geared towards large problems
        crafty: Use defaults geared towards crafted problems
        trendy: Use defaults geared towards industrial problems
        many  : Use default portfolio to configure solver(s)
        <file>: Use configuration file to configure solver(s)
  tester[0..0].share[=None] - Configure physical sharing of constraints [%D]
      %A: {auto|problem|learnt|all}
  tester[0..0].learn_explicit[=None] - Do not use Short Implication Graph for learning
  tester[0..0].sat_prepro[=None] - Run SatELite-like preprocessing (Implicit: %I)
      %A: <level>[,<limit>...]
        <level> : Set preprocessing level to <level  {1..3}>
          1: Variable elimination with subsumption (VE)
          2: VE with limited blocked clause elimination (BCE)
          3: Full BCE followed by VE
        <limit> : [<key {iter|occ|time|frozen|clause}>=]<n> (0=no limit)
          iter  : Set iteration limit to <n>           [0]
          occ   : Set variable occurrence limit to <n> [0]
          time  : Set time limit to <n> seconds        [0]
          frozen: Set frozen variables limit to <n>%%   [0]
          size  : Set size limit to <n>*1000 clauses   [4000]
solve[0..0] - Solve Options
  solve[0..0].solve_limit[=umax,umax] - Stop search after <n> conflicts or <m> restarts

  solve[0..0].parallel_mode[=1,compete] - Run parallel search with given number of threads
      %A: <n {1..64}>[,<mode {compete|split}>]
        <n>   : Number of threads to use in search
        <mode>: Run competition or splitting based search [compete]

  solve[0..0].global_restarts[=no] - Configure global restart policy
      %A: <n>[,<sched>]
        <n> : Maximal number of global restarts (0=disable)
     <sched>: Restart schedule [x,100,1.5] (<type {F|L|x|+}>)

  solve[0..0].distribute[=conflict,global,4,4194303] - Configure nogood distribution [%D]
      %A: <type>[,<mode>][,<lbd {0..127}>][,<size>]
        <type> : Distribute {all|short|conflict|loop} nogoods
        <mode> : Use {global|local} distribution   [global]
        <lbd>  : Distribute only if LBD  <= <lbd>  [4]
        <size> : Distribute only if size <= <size> [-1]
  solve[0..0].integrate[=gp,1024,all] - Configure nogood integration [%D]
      %A: <pick>[,<n>][,<topo>]
        <pick>: Add {all|unsat|gp(unsat wrt guiding path)|active} nogoods
        <n>   : Always keep at least last <n> integrated nogoods   [1024]
        <topo>: Accept nogoods from {all|ring|cube|cubex} peers    [all]

  solve[0..0].enum_mode[=auto] - Configure enumeration algorithm [%D]
      %A: {bt|record|brave|cautious|auto}
        bt      : Backtrack decision literals from solutions
        record  : Add nogoods for computed solutions
        domRec  : Add nogoods over true domain atoms
        brave   : Compute brave consequences (union of models)
        cautious: Compute cautious consequences (intersection of models)
        auto    : Use bt for enumeration and record for optimization
  solve[0..0].project[=no] - Enable projective solution enumeration
      %A: {show|project|auto}[,<bt {0..3}>] (Implicit: %I)
        Project to atoms in show or project directives, or
        select depending on the existence of a project directive
      <bt> : Additional options for enumeration algorithm 'bt'
        Use activity heuristic (1) when selecting backtracking literal
        and/or progress saving (2) when retracting solution literals
  solve[0..0].models[=-1] - Compute at most %A models (0 for all)

  solve[0..0].opt_mode[=opt] - Configure optimization algorithm
      %A: <mode {opt|enum|optN|ignore}>[,<bound>...]
        opt   : Find optimal model
        enum  : Find models with costs <= <bound>
        optN  : Find optimum, then enumerate optimal models
        ignore: Ignore optimize statements
      <bound> : Set initial bound for objective function(s)
asp[0..0] - Asp Options
  asp[0..0].trans_ext[=dynamic] - Configure handling of extended rules
      %A: {all|choice|card|weight|integ|dynamic}
        all    : Transform all extended rules to basic rules
        choice : Transform choice rules, but keep cardinality and weight rules
        card   : Transform cardinality rules, but keep choice and weight rules
        weight : Transform cardinality and weight rules, but keep choice rules
        scc    : Transform "recursive" cardinality and weight rules
        integ  : Transform cardinality integrity constraints
        dynamic: Transform "simple" extended rules, but keep more complex ones
  asp[0..0].eq[=3] - Configure equivalence preprocessing
      Run for at most %A iterations (-1=run to fixpoint)
  asp[0..0].backprop[=0] - Use backpropagation in ASP-preprocessing
  asp[0..0].supp_models[=0] - Compute supported models
  asp[0..0].no_ufs_check[=0] - Disable unfounded set check
  asp[0..0].no_gamma[=0] - Do not add gamma rules for non-hcf disjunctions
  asp[0..0].eq_dfs[=0] - Enable df-order in eq-preprocessing
  asp[0..0].dlp_old_map[=0] - Enable old mapping for disjunctive LPs
solver[0..1] - Solver Options
  solver[0..1].opt_strategy[=bb,lin] - Configure optimization strategy
      %A: {bb|usc}[,<tactics>]
        bb : Model-guided optimization with <tactics {lin|hier|inc|dec}> [lin]
          lin : Basic lexicographical descent
          hier: Hierarchical (highest priority criteria first) descent
          inc : Hierarchical descent with exponentially increasing steps
          dec : Hierarchical descent with exponentially decreasing steps
        usc: Core-guided optimization with <tactics>: <relax>[,<opts>]
          <relax>: Relaxation algorithm {oll|one|k|pmres}                [oll]
            oll    : Use strategy from unclasp
            one    : Add one cardinality constraint per core
            k[,<n>]: Add cardinality constraints of bounded size ([0]=dynamic)
            pmres  : Add clauses of size 3
          <opts> : Tactics <list {disjoint|succinct|stratify}>|<mask {0..7}>
            disjoint: Disjoint-core preprocessing                    (1)
            succinct: No redundant (symmetry) constraints            (2)
            stratify: Stratification heuristic for handling weights  (4)
  solver[0..1].opt_usc_shrink[=no] - Enable core-shrinking in core-guided optimization
      %A: <algo>[,<limit> (0=no limit)]
        <algo> : Use algorithm {lin|inv|bin|rgs|exp|min}
          lin  : Forward linear search unsat
          inv  : Inverse linear search not unsat
          bin  : Binary search
          rgs  : Repeated geometric sequence until unsat
          exp  : Exponential search until unsat
          min  : Linear search for subset minimal core
        <limit>: Limit solve calls to 2^<n> conflicts [10]
  solver[0..1].opt_heuristic[=no] - Use opt. in <list {sign|model}> heuristics
  solver[0..1].restart_on_model[=0] - Restart after each model

  solver[0..1].lookahead[=no] - Configure failed-literal detection (fld)
      %A: <type>[,<limit>] / Implicit: %I
        <type> : Run fld via {atom|body|hybrid} lookahead
        <limit>: Disable fld after <limit> applications ([0]=no limit)
      --lookahead=atom is default if --no-lookback is used

  solver[0..1].heuristic[=vsids,92] - Configure decision heuristic
      %A: {Berkmin|Vmtf|Vsids|Domain|Unit|None}[,<n>]
        Berkmin: Use BerkMin-like heuristic (Check last <n> nogoods [0]=all)
        Vmtf   : Use Siege-like heuristic (Move <n> literals to the front [8])
        Vsids  : Use Chaff-like heuristic (Use 1.0/0.<n> as decay factor  [95])
        Domain : Use domain knowledge in Vsids-like heuristic
        Unit   : Use Smodels-like heuristic (Default if --no-lookback)
        None   : Select the first free variable
  solver[0..1].init_moms[=1] - Initialize heuristic with MOMS-score
  solver[0..1].score_res[=auto] - Resolution score {auto|min|set|multiset}
  solver[0..1].score_other[=all] - Score other learnt nogoods: {auto|no|loop|all}
  solver[0..1].sign_def[=asp] - Default sign: {asp|pos|neg|rnd}
  solver[0..1].sign_fix[=0] - Disable sign heuristics and use default signs only
  solver[0..1].berk_huang[=0] - Enable Huang-scoring in Berkmin
  solver[0..1].vsids_acids[=0] - Enable acids-scheme in Vsids/Domain
  solver[0..1].vsids_progress[=no] - Enable dynamic decaying scheme in Vsids/Domain
      %A: <n>[,<i {1..100}>][,<c>]|(0=disable)
        <n> : Set initial decay factor to 1.0/0.<n>
        <i> : Set decay update to <i>/100.0      [1]
        <c> : Decrease decay every <c> conflicts [5000]
  solver[0..1].nant[=0] - Prefer negative antecedents of P in heuristic
  solver[0..1].dom_mod[=no] - Default modification for domain heuristic
      %A: (no|<mod>[,<pick>])
        <mod>  : Modifier {level|pos|true|neg|false|init|factor}
        <pick> : Apply <mod> to (all | <list {scc|hcc|disj|opt|show}>) atoms
  solver[0..1].save_progress[=160] - Use RSat-like progress saving on backjumps > %A
  solver[0..1].init_watches[=least] - Watched literal initialization: {rnd|first|least}
  solver[0..1].update_mode[=propagate] - Process messages on {propagate|conflict}
  solver[0..1].acyc_prop[=1] - Use backward inference in acyc propagation
  solver[0..1].seed[=1] - Set random number generator's seed to %A
  solver[0..1].no_lookback[=false] - Disable all lookback strategies

  solver[0..1].forget_on_step[=no] - Configure forgetting on (incremental) step
      %A: <list {varScores|signs|lemmaScores|lemmas}>|<mask {0..15}>

  solver[0..1].strengthen[=recursive,all,yes] - Use MiniSAT-like conflict nogood strengthening
      %A: <mode>[,<type>][,<bump {yes|no}>]
        <mode>: Use {local|recursive} self-subsumption check
        <type>: Follow {all|short|binary} antecedents [all]
        <bump>: Bump activities of antecedents        [yes]
  solver[0..1].otfs[=2] - Enable {1=partial|2=full} on-the-fly subsumption
  solver[0..1].update_lbd[=less,0] - Configure LBD updates during conflict resolution
      %A: <mode {less|glucose|pseudo}>[,<n {0..127}>]
        less   : update to X = new LBD   iff X   < previous LBD
        glucose: update to X = new LBD   iff X+1 < previous LBD
        pseudo : update to X = new LBD+1 iff X   < previous LBD
           <n> : Protect updated nogoods on next reduce if X <= <n>
  solver[0..1].update_act[=0] - Enable LBD-based activity bumping
  solver[0..1].reverse_arcs[=0] - Enable ManySAT-like inverse-arc learning
  solver[0..1].contraction[=no] - Configure handling of long learnt nogoods
      %A: <n>[,<rep>]
        <n>  : Contract nogoods if size > <n> (0=disable)
        <rep>: Nogood replacement {no|decisionSeq|allUIP|dynamic} [no]

  solver[0..1].loops[=shared] - Configure learning of loop nogoods
      %A: {common|distinct|shared|no}
        common  : Create loop nogoods for atoms in an unfounded set
        distinct: Create distinct loop nogood for each atom in an unfounded set
        shared  : Create loop formula for a whole unfounded set
        no      : Do not learn loop formulas

  solver[0..1].partial_check[=no] - Configure partial stability tests
      %A: <p>[,<h>] / Implicit: %I
        <p>: Partial check skip percentage
        <h>: Init/update value for high bound ([0]=umax)
  solver[0..1].sign_def_disj[=asp] - Default sign for atoms in disjunctions
  solver[0..1].rand_freq[=0] - Make random decisions with probability %A
  solver[0..1].rand_prob[=no] - Do <n> random searches with [<m>=100] conflicts
  solver[0..1].restarts[=l,60] - Configure restart policy
      %A: <type {D|F|L|x|+}>,<n {1..umax}>[,<args>][,<lim>]
        F,<n>    : Run fixed sequence of <n> conflicts
        L,<n>    : Run Luby et al.'s sequence with unit length <n>
        x,<n>,<f>: Run geometric seq. of <n>*(<f>^i) conflicts  (<f> >= 1.0)
        +,<n>,<m>: Run arithmetic seq. of <n>+(<m>*i) conflicts (<m {0..umax}>)
        ...,<lim>: Repeat seq. every <lim>+j restarts           (<type> != F)
        D,<n>,<f>: Restart based on moving LBD average over last <n> conflicts
                   Mavg(<n>,LBD)*<f> > avg(LBD)
                   use conflict level average if <lim> > 0 and avg(LBD) > <lim>
      no|0       : Disable restarts
  solver[0..1].reset_restarts[=no] - Update restart seq. on model {no|repeat|disable}
  solver[0..1].local_restarts[=1] - Use Ryvchin et al.'s local restarts
  solver[0..1].counter_restarts[=no] - Use counter implication restarts
      %A: (<rate>[,<bump>] | {0|no})
      <rate>: Interval in number of restarts
      <bump>: Bump factor applied to indegrees
  solver[0..1].block_restarts[=no] - Use glucose-style blocking restarts
      %A: <n>[,<R {1.0..5.0}>][,<c>]
        <n>: Window size for moving average (0=disable blocking)
        <R>: Block restart if assignment > average * <R>  [1.4]
        <c>: Disable blocking for the first <c> conflicts [10000]

  solver[0..1].shuffle[=no] - Shuffle problem after <n1>+(<n2>*i) restarts

  solver[0..1].deletion[=basic,50,activity] - Configure deletion algorithm [%D]
      %A: <algo>[,<n {1..100}>][,<sc>]
        <algo>: Use {basic|sort|ipSort|ipHeap} algorithm
        <n>   : Delete at most <n>%% of nogoods on reduction    [75]
        <sc>  : Use {activity|lbd|mixed} nogood scores    [activity]
      no      : Disable nogood deletion
  solver[0..1].del_grow[=no] - Configure size-based deletion policy
      %A: <f>[,<g>][,<sched>] (<f> >= 1.0)
        <f>     : Keep at most T = X*(<f>^i) learnt nogoods with X being the
                  initial limit and i the number of times <sched> fired
        <g>     : Stop growth once T > P*<g> (0=no limit)      [3.0]
        <sched> : Set grow schedule (<type {F|L|x|+}>) [grow on restart]
  solver[0..1].del_cfl[=+,2000,100,20] - Configure conflict-based deletion policy
      %A:   <type {F|L|x|+}>,<args>... (see restarts)
  solver[0..1].del_init[=3,10,4294967285] - Configure initial deletion limit
      %A: <f>[,<n>,<o>] (<f> > 0)
        <f>    : Set initial limit to P=estimated problem size/<f> [%D]
        <n>,<o>: Clamp initial limit to the range [<n>,<n>+<o>]
  solver[0..1].del_estimate[=1] - Use estimated problem complexity in limits
  solver[0..1].del_max[=2000000,0] - Keep at most <n> learnt nogoods taking up to <X> MB
  solver[0..1].del_glue[=2,0] - Configure glue clause handling
      %A: <n {0..15}>[,<m {0|1}>]
        <n>: Do not delete nogoods with LBD <= <n>
        <m>: Count (0) or ignore (1) glue clauses in size limit [0]
  solver[0..1].del_on_restart[=0] - Delete %A%% of learnt nogoods on each restart
configuration[=auto] - Initializes this configuration
      <arg>: {auto|frumpy|jumpy|tweety|handy|crafty|trendy|many|<file>}
        auto  : Select configuration based on problem type
        frumpy: Use conservative defaults
        jumpy : Use aggressive defaults
        tweety: Use defaults geared towards asp problems
        handy : Use defaults geared towards large problems
        crafty: Use defaults geared towards crafted problems
        trendy: Use defaults geared towards industrial problems
        many  : Use default portfolio to configure solver(s)
        <file>: Use configuration file to configure solver(s)
share[=auto] - Configure physical sharing of constraints [%D]
      %A: {auto|problem|learnt|all}
learn_explicit[=0] - Do not use Short Implication Graph for learning
sat_prepro[=no] - Run SatELite-like preprocessing (Implicit: %I)
      %A: <level>[,<limit>...]
        <level> : Set preprocessing level to <level  {1..3}>
          1: Variable elimination with subsumption (VE)
          2: VE with limited blocked clause elimination (BCE)
          3: Full BCE followed by VE
        <limit> : [<key {iter|occ|time|frozen|clause}>=]<n> (0=no limit)
          iter  : Set iteration limit to <n>           [0]
          occ   : Set variable occurrence limit to <n> [0]
          time  : Set time limit to <n> seconds        [0]
          frozen: Set frozen variables limit to <n>%%   [0]
          size  : Set size limit to <n>*1000 clauses   [4000]
stats[=0] - Enable {1=basic|2=full} statistics (<t> for tester)
parse_ext[=false] - Enable extensions in non-aspif input
parse_maxsat[=false] - Treat dimacs input as MaxSAT problem
The heuristics of the solvers in the 'many' portfolio:
  vsids,92
  vsids,0
  berkmin,0
  domain,0
  vsids,0
  vsids,0
  berkmin,512
  vsids,0
  vsids,0
  berkmin,512
  vmtf,0
  vsids,0
  vsids,0
  berkmin,512
  unit,0
  vsids,0
 */

int main(int argc, char** argv)
{

    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");
    std::string programSection = "commonsenseOntology";

    std::string ontology = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/test/ontology.lp");
    std::string facts = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/test/test_facts.lp");
    std::string facets = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/test/ontology_properties_facets.lp");
    std::string ontology_rules = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/test/ontology_rules.lp");

    reasoner::asp::Solver* solver = new reasoner::asp::Solver({});

    /**
     * ################# Ontology #################
     */

    auto start = std::chrono::high_resolution_clock::now();
    solver->add("", {}, (ontology + facets).c_str());
    auto finish = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Adding runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{"", {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Grounding runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    for (Clingo::SymbolicAtom atom : solver->clingo->symbolic_atoms()) {
        if (!atom.is_external()) {
            continue;
        }
        solver->assignExternal(atom.symbol(), Clingo::TruthValue::True);
    }
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "External setting runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    start = std::chrono::high_resolution_clock::now();
    solver->add((programSection + "rules").c_str(), {}, (facts + ontology_rules).c_str());
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Adding rules runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{(programSection + "rules").c_str(), {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    std::cout << "Grounding rules runtime: " << duration / 1000.0 << " ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

    std::cout << "Solving runtime: " << duration / 1000.0 << " ms" << std::endl;

    auto models = solver->getCurrentModels();
    std::stringstream ss;
    Agraph_t* g;
    static GVC_t* gvc;
    if (!gvc) {
        gvc = gvContext();
    }
    /* Create a simple digraph */
    g = agopen(const_cast<char*>("cn5_Ontology"), Agdirected, nullptr);
    agsafeset(g, const_cast<char*>("rankdir"), const_cast<char*>("LR"), const_cast<char*>(""));
    ss << "Solution: ";
    for (size_t i = 0; i < models.size(); i++) {
        ss << "Model number " << i + 1 << ":\n" << std::endl;
        for (auto atom : models.at(i)) {
            ss << atom << " ";
            cnoe::OntologyExtractor::generateOntologyGraph(g, atom.to_string());
        }
        ss << std::endl;
    }
    gvLayout(gvc, g, "dot");
    /* Output in .dot format */
    FILE* fptr;
    std::string dot = domain_config_folder + "/ontologies/test/classification.dot";
    fptr = fopen(dot.c_str(), "w");
    gvRender(gvc, g, "dot", fptr);
    fclose(fptr);
    gvFreeLayout(gvc, g);
    agclose(g);

    auto statistics = solver->clingo->statistics();
    ss << "\nSolve Statistics:" << std::endl;
    ss << "TOTAL Time: " << (statistics["summary"]["times"]["total"] * 1000.0) << " ms" << std::endl;
    ss << "CPU Time: " << (statistics["summary"]["times"]["cpu"] * 1000.0) << " ms" << std::endl;
    ss << "SAT Time: " << (statistics["summary"]["times"]["sat"] * 1000.0) << " ms" << std::endl;
    ss << "UNSAT Time: " << (statistics["summary"]["times"]["unsat"] * 1000.0) << " ms" << std::endl;
    ss << "SOLVE Time: " << (statistics["summary"]["times"]["solve"] * 1000.0) << " ms" << std::endl;

    std::cout << ss.str() << std::endl;

    // system("dot -Tps test.dot -o outfile.ps");
    std::string pdf = domain_config_folder + "/ontologies/test/classification.pdf";
    std::system(std::string("dot -Tpdf " + dot + " -o " + pdf).c_str());
}