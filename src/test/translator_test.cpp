#include <iostream>
#include "asp/ASPTranslator.h"

int main(int argc, char** argv) {
    asp::ASPTranslator aspTranslator;
    std::cout << aspTranslator.generateOntologyRules(true) << std::endl;

    return EXIT_SUCCESS;
}