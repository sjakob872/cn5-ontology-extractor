#include "OntologyExtractor.h"

#include "Ontology.h"

#include <reasoner/asp/Solver.h>

#include <chrono>
#include <fstream>
#include <string>

int main(int argc, char** argv)
{
    int iterations = std::stoi(std::string(argv[1]));

    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");
    std::string programSection = "commonsenseOntology";

    std::ofstream resultsFile;

    resultsFile.open(domain_config_folder + "/evaluation/pizza_" + std::to_string(iterations) + ".csv", std::ios_base::app);

    resultsFile << "rule add,rule ground,externals,solve,query add,query ground,query solve\n" << std::flush;

    std::string ontology = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/PizzaASP/Pizza.lp");
    std::string individuals = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/PizzaASP/Individuals.lp");
    std::string facets = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/PizzaASP/Facets.lp");
    std::string rules = cnoe::OntologyExtractor::readFromFile(domain_config_folder + "/ontologies/PizzaASP/Rules.lp");

    for (int i = 0; i < iterations; i++) {
        auto solver = new reasoner::asp::Solver({});

        /**
         * ################# Ontology + Facets Solving #################
         **/

        auto start = std::chrono::high_resolution_clock::now();
        solver->add("", {}, (ontology + facets).c_str());
        auto finish = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        start = std::chrono::high_resolution_clock::now();
        solver->ground({{"", {}}}, nullptr);
        finish = std::chrono::high_resolution_clock::now();

        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        start = std::chrono::high_resolution_clock::now();
        for (Clingo::SymbolicAtom atom : solver->clingo->symbolic_atoms()) {
            if (!atom.is_external()) {
                continue;
            }
            solver->assignExternal(atom.symbol(), Clingo::TruthValue::True);
        }
        finish = std::chrono::high_resolution_clock::now();

        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        start = std::chrono::high_resolution_clock::now();
        solver->solve();
        finish = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        /**
         * ################# Query #################
         **/

        start = std::chrono::high_resolution_clock::now();
        solver->add((programSection + "rules").c_str(), {}, (individuals + rules).c_str());
        finish = std::chrono::high_resolution_clock::now();

        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        start = std::chrono::high_resolution_clock::now();
        solver->ground({{(programSection + "rules").c_str(), {}}}, nullptr);
        finish = std::chrono::high_resolution_clock::now();

        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "," << std::flush;

        start = std::chrono::high_resolution_clock::now();
        solver->solve();
        finish = std::chrono::high_resolution_clock::now();

        duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();

        resultsFile << duration << "\n" << std::flush;

        std::cout << "Iteration " << i + 1 << " out of " << iterations << " finished." << std::endl;
        delete solver;
    }
}