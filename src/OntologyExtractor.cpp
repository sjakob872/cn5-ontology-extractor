#include "OntologyExtractor.h"

#include "AnswerGraph.h"
#include "Ontology.h"
#include "asp/ASPTranslator.h"
#include "conceptnet/Concept.h"
#include "conceptnet/ConceptNet.h"
#include "conceptnet/Edge.h"

#include <asp/OntologyTerms.h>
#include <chrono>
#include <fstream>
#include <gvc.h>
#include <gvcext.h>
#include <iostream>
#include <reasoner/asp/Solver.h>
#include <unordered_set>

namespace cnoe
{
OntologyExtractor::OntologyExtractor()
{
    this->translator = new asp::ASPTranslator();
    this->conceptNet = new conceptnet::ConceptNet();
    this->finishedExtracting = false;
    this->answerGraph = nullptr;
    this->ontology = nullptr;
    this->IS_A_WEIGHT = 1.0;
    this->FORM_OF_WEIGHT = 2.0;
    this->SYNONYM_WEIGHT = 2.0;
    this->MIN_RELATEDNESS = 0.7;
    this->HAS_PROPERTY_WEIGHT = 1.5;
}

OntologyExtractor::~OntologyExtractor()
{
    if (this->translator != nullptr) {
        delete this->translator;
    }
    delete this->conceptNet;
    delete this->answerGraph;
    delete this->ontology;
}

void OntologyExtractor::renderDot(cnoe::AnswerGraph* answerGraph, const std::string& domainFolder)
{
    Agraph_t* g;
    /* set up a graphviz context - but only once even for multiple graphs */
    static GVC_t* gvc;
    if (!gvc) {
        gvc = gvContext();
    }
    /* Create a simple digraph */
    g = agopen(const_cast<char*>("cn5_Ontology"), Agdirected, nullptr);
    agsafeset(g, const_cast<char*>("rankdir"), const_cast<char*>("RL"), const_cast<char*>(""));
    answerGraph->renderDot(g, false);

    /* Set an attribute - in this case one that affects the visible rendering */
    /* Use the directed graph layout engine */
    gvLayout(gvc, g, "dot");
    /* Output in .dot format */
    FILE* fptr;
    std::string dot = domainFolder + "/ontologies/test/ontology.dot";
    fptr = fopen(dot.c_str(), "w");
    gvRender(gvc, g, "dot", fptr);
    fclose(fptr);
    gvFreeLayout(gvc, g);
    agclose(g);

    std::string pdf = domainFolder + "/ontologies/test/ontology.pdf";
    std::system(std::string("dot -Tpdf " + dot + " -o " + pdf).c_str());

    // call this to translate into ps format and open with evince
    //    dot -Tps ~/test.dot -o outfile.ps
}

std::string OntologyExtractor::readFromFile(const std::string& path)
{
    std::ifstream file;
    file.open(path);

    if (!file) {
        std::cerr << "OntologyExtractor: Unable to open file" << std::endl;
        exit(EXIT_FAILURE); // call system to stop
    }
    std::string ret;
    std::string tmp;
    while (!file.eof()) // To get you all the lines.
    {
        getline(file, tmp);
        ret.append(tmp);
    }
    file.close();
    return ret;
}

void OntologyExtractor::extractOntology(const std::string& input)
{
#ifdef ONTOLOGY_DEBUG
    int counter = 0;
#endif
    double level = 0.0;
    auto start = std::chrono::high_resolution_clock::now();
    this->answerGraph = new cnoe::AnswerGraph();
    conceptnet::Concept* root = conceptnet::ConceptNet::getConcept(this->answerGraph, input);
    this->answerGraph->setRoot(root);
    std::vector<conceptnet::Edge*> edges;
    std::unordered_set<conceptnet::Concept*> visited;
    std::vector<conceptnet::Concept*> firstFringe;
    std::set<conceptnet::Concept*> secondFringe;
    conceptnet::Concept* currentConcept;
    firstFringe.push_back(root);
    // std::cout << "OntologyExtractor: Starting to crawl CN5." << std::endl;
    do {
        secondFringe.clear();
        while (!firstFringe.empty()) {
            currentConcept = firstFringe.front();
            edges = conceptnet::ConceptNet::getEdges(this->answerGraph, conceptnet::Relation::IsA, currentConcept->term, -1, this->IS_A_WEIGHT);
            auto tmp = conceptnet::ConceptNet::getEdges(this->answerGraph, conceptnet::Relation::FormOf, currentConcept->term, -1, this->FORM_OF_WEIGHT);
            edges.insert(edges.end(), tmp.begin(), tmp.end());
            currentConcept->addEdges(edges);
            visited.insert(currentConcept);
#ifdef ONTOLOGY_DEBUG
            if (counter < 2) {
#endif
                for (conceptnet::Edge* edge : edges) {

                    if (visited.find(edge->fromConcept) == visited.end() || visited.find(edge->toConcept) == visited.end()) {
                        if (secondFringe.find(edge->fromConcept) == secondFringe.end()) {
                            if (edge->level < level) {
                                edge->level = level;
                            }
                            edge->relatedness = conceptnet::ConceptNet::getRelatedness(edge->fromConcept->term, edge->toConcept->term);
                            secondFringe.insert(edge->fromConcept);
                            secondFringe.insert(edge->toConcept);
                        }
                    }
                }
#ifdef ONTOLOGY_DEBUG
            }
#endif
            firstFringe.erase(firstFringe.begin());
#ifdef ONTOLOGY_DEBUG
            counter++;
#endif
        }
        // level = level >= 2.0 ? level - 2.0 : 0.0;
        firstFringe.insert(firstFringe.begin(), secondFringe.begin(), secondFringe.end());
    } while (!secondFringe.empty());

    for (auto pair : this->answerGraph->getConcepts()) {
        auto synonymEdges = conceptnet::ConceptNet::getEdges(this->answerGraph, conceptnet::Relation::Synonym, pair.second->term, -1, this->SYNONYM_WEIGHT);
        for (conceptnet::Edge* edge : synonymEdges) {
            double relatedness = conceptnet::ConceptNet::getRelatedness(edge->fromConcept->term, edge->toConcept->term);
            if (relatedness < this->MIN_RELATEDNESS) {
                edge->relatedness = -1;
                continue;
            }
            edge->relatedness = relatedness;
            pair.second->addEdge(edge);
        }
    }

    for (auto pair : this->answerGraph->getConcepts()) {
        pair.second->addEdges(conceptnet::ConceptNet::getOutgoingEdges(
                this->answerGraph, conceptnet::Relation::HasProperty, pair.second->term, -1, this->HAS_PROPERTY_WEIGHT));
    }

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    // std::cout << "OntologyExtractor: Ontology extraction runtime: " << duration / 1000000.0 << " s" << std::endl;
}

void OntologyExtractor::generateASPOntology()
{
    std::string domain_config_folder = ::getenv("DOMAIN_CONFIG_FOLDER");

    // std::cout << "OntologyExtractor: Finished crawling CN5. Starting to write LP." << std::endl;

    auto start = std::chrono::high_resolution_clock::now();
    this->ontology = asp::ASPTranslator::extractOntology(this->answerGraph, true);
    this->ontology->rules = this->translator->generateOntologyRules(true);

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    // std::cout << "OntologyExtractor: Ontology creation runtime: " << duration / 1000.0 << " ms" << std::endl;

    std::ofstream ontologyFile;
    ontologyFile.open(domain_config_folder + "/ontologies/test/ontology.lp");
    ontologyFile << this->ontology->ontology;
    ontologyFile.close();

    std::ofstream ruleFile;
    ruleFile.open(domain_config_folder + "/ontologies/test/ontology_rules.lp");
    ruleFile << this->ontology->rules;
    ruleFile.close();

    // std::cout << "OntologyExtractor: Finished writing LP." << std::endl;

    // conceptNet->findInconsistencies(answerGraph);
    // renderDot(answerGraph);
    // system("dot -Tps test.dot -o outfile.ps");
    // system("dot -Tpdf test.dot -o outfile.pdf");
}

void OntologyExtractor::generateOntologyGraph(Agraph_t* g, const std::string& atom)
{
    std::size_t posOpeningBrace = atom.find('(');
    std::size_t posClosingBrace = atom.find(')');
    std::size_t posFirstComma = atom.find(',');
    if (atom.find("subSetOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        std::size_t posThirdComma = atom.find(',', posSecondComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"),
                const_cast<char*>(("subSetOf[" + atom.substr(posSecondComma + 1, posThirdComma - posSecondComma - 1) + ", " +
                                   atom.substr(posThirdComma + 1, posClosingBrace - posThirdComma - 1) + "]")
                                          .c_str()),
                const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("green"), const_cast<char*>(""));
    } else if (atom.find("classifiedAs") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"),
                const_cast<char*>(("classifiedAs[" + atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1) + "]").c_str()),
                const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("red"), const_cast<char*>(""));
    } else if (atom.find("hasPropertyValue") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"),
                const_cast<char*>(("hasPropertyValue[" + atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1) + "]").c_str()),
                const_cast<char*>(""));
    } else if (atom.find("hasProperty") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"),
                const_cast<char*>(("hasProperty[" + atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1) + "]").c_str()),
                const_cast<char*>(""));
    } else if (atom.find("propertyInheritedFrom") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        std::size_t posThirdComma = atom.find(',', posSecondComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posSecondComma + 1, posThirdComma - posSecondComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"),
                const_cast<char*>(("propertyInheritedFrom[" + atom.substr(posThirdComma + 1, posClosingBrace - posThirdComma - 1) + "]").c_str()),
                const_cast<char*>(""));
    } else if (atom.find("typeOf") != std::string::npos && atom.find("facet") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        std::size_t posThirdComma = atom.find(',', posSecondComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posThirdComma + 1, atom.size() - posClosingBrace - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("typeOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("valueRangeOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("valueRangeOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("hasFacet") != std::string::npos) {
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posClosingBrace - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("hasFacet"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("facetInheritedFrom") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("facetInheritedFrom"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("facetOf") != std::string::npos) {
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posClosingBrace - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("facetOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("typeOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("typeOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("subPropertyOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("subPropertyOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("domainInheritedFrom") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        std::size_t posThirdComma = atom.find(',', posSecondComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posSecondComma + 1, posThirdComma - posSecondComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("domainInheritedFrom"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("cyan"), const_cast<char*>(""));
    } else if (atom.find("hasDomain") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("hasDomain"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("cyan"), const_cast<char*>(""));
    } else if (atom.find("domainOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("domainOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("cyan"), const_cast<char*>(""));
    } else if (atom.find("typeOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("typeOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("blue"), const_cast<char*>(""));
    } else if (atom.find("domainOf") != std::string::npos) {
        std::size_t posSecondComma = atom.find(',', posFirstComma + 1);
        Agnode_t* from = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()), TRUE);
        Agnode_t* to = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1).c_str()), TRUE);
        Agedge_t* ed = agedge(g, from, to, nullptr, TRUE);
        agsafeset(ed, const_cast<char*>("label"), const_cast<char*>("domainOf"), const_cast<char*>(""));
        agsafeset(ed, const_cast<char*>("color"), const_cast<char*>("cyan"), const_cast<char*>(""));
    } else if (atom.find("propertyViolation") != std::string::npos || atom.find("is") != std::string::npos || atom.find("hasValue") != std::string::npos) {
        return;
    } else {
        std::cout << "\033[1;31mOntologyExtractor: atom not supported: " << atom << "\033[0m" << std::endl;
        return;
        /*Agnode_t* origin = agnode(g, const_cast<char*>(atom.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1).c_str()),
        TRUE); agsafeset(origin, "color", "green", ""); Agnode_t* is = agnode(g, const_cast<char*>(atom.substr(posFirstComma + 1,
        posClosingBrace - posFirstComma - 1).c_str()), TRUE); Agedge_t* ed = agedge(g, origin, is, NULL, TRUE);
        agsafeset(ed, "label", "is", "");*/
    }
}

void OntologyExtractor::extract(const std::string& rootConcept)
{
    std::string domainConfigFolder = ::getenv("DOMAIN_CONFIG_FOLDER");
    this->extractOntology(rootConcept);
    this->generateASPOntology();
    this->finishedExtracting = true;
    // cnoe::OntologyExtractor::renderDot(this->answerGraph, domainConfigFolder);
}

AnswerGraph* OntologyExtractor::getAnswerGraph() const
{
    return this->answerGraph;
}

Ontology* OntologyExtractor::getOntology() const
{
    return this->ontology;
}

bool OntologyExtractor::isFinishedExtracting() const
{
    return this->finishedExtracting;
}

void OntologyExtractor::extractOntologyFromString(const std::string& ontology)
{
    this->answerGraph = new cnoe::AnswerGraph();
    this->ontology = new Ontology();
    this->ontology->rules = this->translator->generateOntologyRules(true);

    std::vector<std::string> ontologyLines = getLines(ontology);
    //#external cs_isA("object", "thing", 0).
    size_t posFirstComma = ontologyLines.at(0).find_first_of(',');
    size_t posSecondComma;
    size_t posUnderscore;
    size_t posOpeningBrace;
    size_t posClosingBrace;
    size_t posFirstQuote;
    size_t posSecondQuote;
    size_t posCNPrefix;
    size_t posDot;
    size_t posThirdQuote = ontologyLines.at(0).find_first_of('"', posFirstComma + 1);
    size_t posFourthQuote = ontologyLines.at(0).find_first_of('"', posThirdQuote + 1);
    std::string conceptName = ontologyLines.at(0).substr(posThirdQuote + 1, posFourthQuote - posThirdQuote - 1);
    this->answerGraph->setRoot(this->answerGraph->createConcept(conceptName, conceptName, ""));

    std::string currentLine;
    std::string startTerm;
    std::string endTerm;
    std::string relation;
    std::string weight;
    std::string aspId;
    std::string timeStamp;
    for (int i = 0; i < ontologyLines.size(); i += 2) {
        currentLine = ontologyLines.at(i);
        this->ontology->ontology.append(currentLine).append("\n");
        posCNPrefix = currentLine.find(asp::OntologyTerms::CONCEPTNET_PREFIX);
        posDot = currentLine.find_last_of('.');
        posUnderscore = currentLine.find_first_of('_');
        posOpeningBrace = currentLine.find_first_of('(', posUnderscore + 1);
        posFirstQuote = currentLine.find_first_of('"', posOpeningBrace + 1);
        posSecondQuote = currentLine.find_first_of('"', posFirstQuote + 1);
        posThirdQuote = currentLine.find_first_of('"', posSecondQuote + 1);
        posFourthQuote = currentLine.find_first_of('"', posThirdQuote + 1);
        this->ontology->externals.push_back(currentLine.substr(posCNPrefix, posDot - posCNPrefix));
        relation = currentLine.substr(posUnderscore + 1, posOpeningBrace - posUnderscore - 1);
        startTerm = currentLine.substr(posFirstQuote + 1, posSecondQuote - posFirstQuote - 1);
        endTerm = currentLine.substr(posThirdQuote + 1, posFourthQuote - posThirdQuote - 1);

        currentLine = ontologyLines.at(i + 1);
        this->ontology->ontology.append(currentLine).append("\n");
        posOpeningBrace = currentLine.find_first_of('(');
        posFirstComma = currentLine.find_first_of(',', posOpeningBrace + 1);
        posSecondComma = currentLine.find_first_of(',', posFirstComma + 1);
        posClosingBrace = currentLine.find_first_of(')', posSecondComma + 1);

        aspId = trim(currentLine.substr(posOpeningBrace + 1, posFirstComma - posOpeningBrace - 1));
        weight = trim(currentLine.substr(posFirstComma + 1, posSecondComma - posFirstComma - 1));
        timeStamp = trim(currentLine.substr(posSecondComma + 1, posClosingBrace - posSecondComma - 1));

        conceptnet::Concept* fromConcept = this->answerGraph->createConcept(startTerm, startTerm, "");
        conceptnet::Concept* toConcept = this->answerGraph->createConcept(endTerm, endTerm, "");
        std::string edgeId = "/a[" + relation + "," + startTerm + "," + endTerm + "]"; // dont use conceptnet's edgeId, its evil!
        conceptnet::Edge* edge =
                this->answerGraph->createEdge(edgeId, "en", fromConcept, toConcept, conceptnet::ConceptNet::getRelation(relation), std::stod(weight) / 100.0);
        long id = std::stol(aspId);
        this->answerGraph->updateASPID(id);
        edge->aspId = id;
        edge->timeStamp = std::stol(timeStamp);
        fromConcept->addEdge(edge);
    }

    this->finishedExtracting = true;
}

std::vector<std::string> OntologyExtractor::getLines(std::string ontology)
{
    std::vector<std::string> ret;
    std::stringstream stringStream(ontology);
    std::string line;
    while (std::getline(stringStream, line, '\n')) {
        if (line.empty()) {
            continue;
        }
        ret.push_back(line);
    }
    return ret;
}

std::string OntologyExtractor::trim(const std::string& str)
{
    const std::string& whitespace = " \t";
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        return ""; // no content
    }
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::vector<std::string> OntologyExtractor::solve(std::string individuals, std::pair<std::string, std::string> facets, std::string solutionPath)
{
    /**
     * ################# ADD #################
     */
    this->statisitcs = std::vector<int>(5,0);
    auto start = std::chrono::high_resolution_clock::now();
    auto solver = new reasoner::asp::Solver({});

    solver->add("", {}, (this->ontology->ontology + facets.first).c_str());

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    this->statisitcs.at(0) = duration;
    std::cout << "Main: Adding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Ground #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->ground({{"", {}}}, nullptr);
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    this->statisitcs.at(1) = duration;
    std::cout << "Main: Grounding runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Assign Externals #################
     */

    for (Clingo::SymbolicAtom atom : solver->clingo->symbolic_atoms()) {
        if (!atom.is_external()) {
            continue;
        }
        solver->assignExternal(atom.symbol(), Clingo::TruthValue::True);
    }
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    this->statisitcs.at(2) = duration;
    std::cout << "Main: External runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Solve #################
     */
    start = std::chrono::high_resolution_clock::now();
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    this->statisitcs.at(3) = duration;
    std::cout << "Main: Solving runtime: " << duration / 1000.0 << " ms" << std::endl;

    /**
     * ################# Query #################
     */

    start = std::chrono::high_resolution_clock::now();
    solver->add("test", {}, (this->ontology->rules + individuals + facets.second).c_str());
    solver->ground({{"test", {}}}, nullptr);
    solver->solve();
    finish = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
    std::cout << "Main: Query runtime: " << duration / 1000.0 << " ms" << std::endl;
    this->statisitcs.at(4) = duration;
    auto models = solver->getCurrentModels();
    Agraph_t* g;
    static GVC_t* gvc;
    if (!gvc) {
        gvc = gvContext();
    }
    /* Create a simple digraph */
    g = agopen(const_cast<char*>("cn5_Ontology"), Agdirected, nullptr);
    agsafeset(g, const_cast<char*>("rankdir"), const_cast<char*>("LR"), const_cast<char*>(""));
    std::vector<std::string> ret;
    std::stringstream stringStream;
    for (size_t i = 0; i < models.size(); i++) {
        for (auto atom : models.at(i)) {
            stringStream << atom << " ";
            cnoe::OntologyExtractor::generateOntologyGraph(g, atom.to_string());
        }
        stringStream << std::endl;
        ret.push_back(stringStream.str());
        stringStream.str("");
    }
    gvLayout(gvc, g, "dot");
    /* Output in .dot format */
    FILE* fptr;
    std::string dot = solutionPath + "classification.dot";
    fptr = fopen(dot.c_str(), "w");
    gvRender(gvc, g, "dot", fptr);
    fclose(fptr);
    gvFreeLayout(gvc, g);
    agclose(g);

    std::string pdf = solutionPath + "classification.pdf";
    std::system(std::string("dot -Tpdf -Gdpi=600 " + dot + " -o " + pdf).c_str());
    delete solver;
    return ret;
}

void OntologyExtractor::createEmptyOntology()
{
    this->answerGraph = new cnoe::AnswerGraph();
    this->ontology = new Ontology();
    this->ontology->rules = this->translator->generateOntologyRules(true);
}

std::vector<std::string> OntologyExtractor::getOntologyRelations()
{
    std::vector<std::string> ret;
    ret.emplace_back(conceptnet::relations[conceptnet::Relation::IsA]);
    ret.emplace_back(conceptnet::relations[conceptnet::Relation::FormOf]);
    ret.emplace_back(conceptnet::relations[conceptnet::Relation::Synonym]);
    ret.emplace_back(conceptnet::relations[conceptnet::Relation::HasProperty]);
    return ret;
}

long OntologyExtractor::addEdge(std::string fromConceptString, std::string relation, std::string toConceptString, double weight, long id)
{
    conceptnet::Concept* fromConcept = this->answerGraph->createConcept(fromConceptString, fromConceptString, "");
    conceptnet::Concept* toConcept = this->answerGraph->createConcept(toConceptString, toConceptString, "");
    std::string edgeId = generateEdgeId(fromConceptString, relation, toConceptString); // dont use conceptnet's edgeId, its evil!
    conceptnet::Edge* edge = this->answerGraph->createEdge(edgeId, "en", fromConcept, toConcept, conceptnet::ConceptNet::getRelation(relation), weight);
    edge->aspId = id == -1 ? this->answerGraph->getNextASPID() : id;
    edge->timeStamp = 0;
    fromConcept->addEdge(edge);
    asp::ASPTranslator::translateEdge(this->answerGraph, this->ontology, edge, true);
    return edge->aspId;
}

std::string OntologyExtractor::generateEdgeId(const std::string& fromConceptString, const std::string& relation, const std::string& toConceptString)
{
    std::string lowerRelation = relation;
    lowerRelation[0] = std::tolower(lowerRelation[0]);
    return "/a[" + lowerRelation + "," + fromConceptString + "," + toConceptString + "]";
}

void OntologyExtractor::deleteEdge(std::string fromConceptString, std::string relation, std::string toConceptString, long id)
{
    this->answerGraph->removeEdge(fromConceptString, relation, toConceptString);
    asp::ASPTranslator::extractOntology(this->answerGraph, this->ontology, true);
}

void OntologyExtractor::editEdge(std::string oldStartConcept, std::string newStartConcept, std::string oldRelation, std::string newRelation,
        std::string oldEndConcept, std::string newEndConcept, double weight, long id)
{
    this->answerGraph->removeEdge(oldStartConcept, oldRelation, oldEndConcept);
    this->addEdge(newStartConcept, newRelation, newEndConcept, weight, id);
    asp::ASPTranslator::extractOntology(this->answerGraph, this->ontology, true);
}

conceptnet::Concept* OntologyExtractor::createConcept(std::string conceptName)
{
    return this->answerGraph->createConcept(conceptName, conceptName, "");
    ;
}

double OntologyExtractor::getIsAWeight() const
{
    return this->IS_A_WEIGHT;
}

void OntologyExtractor::setIsAWeight(double isAWeight)
{
    this->IS_A_WEIGHT = isAWeight;
}

double OntologyExtractor::getFormOfWeight() const
{
    return this->FORM_OF_WEIGHT;
}

void OntologyExtractor::setFormOfWeight(double formOfWeight)
{
    this->FORM_OF_WEIGHT = formOfWeight;
}

double OntologyExtractor::getSynonymWeight() const
{
    return this->SYNONYM_WEIGHT;
}

void OntologyExtractor::setSynonymWeight(double synonymWeight)
{
    this->SYNONYM_WEIGHT = synonymWeight;
}

double OntologyExtractor::getMinRelatedness() const
{
    return MIN_RELATEDNESS;
}

void OntologyExtractor::setMinRelatedness(double minRelatedness)
{
    this->MIN_RELATEDNESS = minRelatedness;
}

double OntologyExtractor::getHasPropertyWeight() const
{
    return this->HAS_PROPERTY_WEIGHT;
}

void OntologyExtractor::setHasPropertyWeight(double hasPropertyWeight)
{
    this->HAS_PROPERTY_WEIGHT = hasPropertyWeight;
}

std::vector<int> OntologyExtractor::getStatistics()
{
    return this->statisitcs;
}
} // namespace cnoe