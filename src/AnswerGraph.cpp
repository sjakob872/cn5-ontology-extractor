#include "AnswerGraph.h"
#include "OntologyExtractor.h"
#include "conceptnet/Concept.h"
#include "conceptnet/ConceptPath.h"
#include "conceptnet/Edge.h"

#include <gvc.h>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace cnoe
{
AnswerGraph::AnswerGraph()
        : utilitiesCalculated(false)
{
    this->root = nullptr;
    this->counter = -1;
}

AnswerGraph::~AnswerGraph()
{
    for (auto& edgeEntry : edges) {
        delete edgeEntry.second;
    }
    for (auto& conceptEntry : concepts) {
        delete conceptEntry.second;
    }
    for (auto conceptPath : answerPaths) {
        delete conceptPath;
    }
}

void AnswerGraph::setRoot(conceptnet::Concept* r)
{
    this->root = r;
}

std::vector<conceptnet::Concept*> AnswerGraph::getBestAnswers(size_t maxNumberOfAnswers)
{
    this->calculateUtilities();

    std::vector<conceptnet::Concept*> bestConcepts;
    for (auto utilityEntry : this->utilities) {
        if (bestConcepts.size() < maxNumberOfAnswers) {
            bestConcepts.push_back(utilityEntry.first);
            continue;
        }

        for (size_t i = 0; i < bestConcepts.size(); i++) {
            conceptnet::Concept* currentBestConcept = bestConcepts[i];
            if (this->utilities[currentBestConcept] < utilityEntry.second) {
                bestConcepts.push_back(utilityEntry.first);
                bestConcepts.erase(bestConcepts.begin() + i);
                break;
            }
        }
    }

    std::sort(bestConcepts.begin(), bestConcepts.end(),
            [this](conceptnet::Concept* a, conceptnet::Concept* b) -> bool { return this->utilities[a] < this->utilities[b]; });

    return bestConcepts;
}

void AnswerGraph::calculateUtilities()
{
    if (utilitiesCalculated) {
        return;
    }
    for (conceptnet::ConceptPath* conceptPath : this->answerPaths) {
        conceptPath->calculateUtility();
    }

    for (conceptnet::Concept* concept : this->answerConcepts) {
        double utility = 0;
        for (conceptnet::ConceptPath* conceptPath : this->answerPaths) {
            if (conceptPath->getEnd() == concept) {
                utility += conceptPath->getUtility();
            }
        }
        this->utilities.emplace(concept, utility);
    }
}

void AnswerGraph::markInconsistentEdges()
{
    for (const auto& adjectiveAntonymPair : this->adjectiveAntonymMap) {
        if (adjectiveAntonymPair.second.empty()) {
            continue;
        }
        std::cout << "markInconsistentEdges: adj " << adjectiveAntonymPair.first << std::endl;
        conceptnet::Concept* adjective = this->getConcept("/c/en/" + adjectiveAntonymPair.first->term);
        std::vector<conceptnet::Concept*> closed;
        for (conceptnet::Edge* edge : adjectiveAntonymPair.second) {
            conceptnet::Concept* antonym;
            if (edge->fromConcept == adjective) {
                antonym = edge->toConcept;
            } else {
                antonym = edge->fromConcept;
            }
            if (std::find(closed.begin(), closed.end(), antonym) != closed.end()) {
                continue;
            }
            std::vector<conceptnet::Edge*> adjectiveEdges = this->getEdges(this->root, adjective);
            double adjectiveWeight = 0;
            for (conceptnet::Edge* adjectiveEdge : adjectiveEdges) {
                if (adjectiveEdge->causesInconsistency) {
                    continue;
                }
                adjectiveWeight += adjectiveEdge->weight;
            }
            std::vector<conceptnet::Edge*> antonymEdges = this->getEdges(this->root, antonym);
            double antonymWeight = 0;
            for (conceptnet::Edge* antonymEdge : antonymEdges) {
                if (antonymEdge->causesInconsistency) {
                    continue;
                }
                antonymWeight += antonymEdge->weight;
            }
            std::cout << "markInconsistentEdges: adj weight " << adjectiveWeight << " antonym weight " << antonymWeight << std::endl;
            if (adjectiveWeight >= antonymWeight) {
                for (conceptnet::Edge* antonymEdge : antonymEdges) {
                    std::cout << "markInconsistentEdges: " << antonymEdge->toString() << std::endl;
                    antonymEdge->causesInconsistency = true;
                }
            } else {
                for (conceptnet::Edge* adjectiveEdge : adjectiveEdges) {
                    std::cout << "markInconsistentEdges: " << adjectiveEdge->toString() << std::endl;
                    adjectiveEdge->causesInconsistency = true;
                }
            }
            closed.push_back(antonym);
        }
    }
}

std::string AnswerGraph::toString() const
{
    std::stringstream ret;
    if (root != nullptr) {
        ret << "root: " << root->term << std::endl;
    } else {
        ret << "root: undefined!" << std::endl;
    }
    ret << "edges: " << std::endl;
    for (auto edge : this->edges) {
        ret << "\t" << edge.second->toString() << std::endl;
    }
    return ret.str();
}

void AnswerGraph::renderDot(Agraph_t* g, bool markInconsistencies)
{
    std::vector<conceptnet::Concept*> openNodes;
    std::vector<conceptnet::Concept*> closedNodes;

    Agnode_t* node = agnode(g, const_cast<char*>(this->root->term.c_str()), TRUE);
    agsafeset(node, const_cast<char*>("color"), const_cast<char*>("green"), const_cast<char*>(""));

    openNodes.push_back(this->root);

    if (!markInconsistencies) {
        while (!openNodes.empty()) {
            conceptnet::Concept* openNode = openNodes[0];
            // std::cout << "AnswerGraph:renderDot: " << node->term << " " << node << std::endl;
            openNodes.erase(openNodes.begin());
            if (std::find(closedNodes.begin(), closedNodes.end(), openNode) != closedNodes.end()) {
                continue;
            }
            closedNodes.push_back(openNode);

            for (const conceptnet::Edge* edge : openNode->getEdges()) {
                generateEdge(g, openNodes, openNode->term, edge);
            }
        }

        for (conceptnet::Concept* concept : this->getBestAnswers(5)) {
            Agnode_t* n = agnode(g, const_cast<char*>(concept->term.c_str()), TRUE);
            Agnode_t* weightNode = agnode(g, const_cast<char*>(std::to_string(this->utilities[concept]).c_str()), TRUE);
            agedge(g, weightNode, n, const_cast<char*>("weight"), TRUE);
            agsafeset(n, const_cast<char*>("color"), const_cast<char*>("red"), const_cast<char*>(""));
        }

    } else {
        for (const auto& pair : this->adjectiveAntonymMap) {
            if (pair.second.empty()) {
                continue;
            }
            for (conceptnet::Edge* edge : this->root->getEdges()) {
                if (edge->fromConcept == pair.first || edge->toConcept == pair.first) {
                    generateEdge(g, openNodes, this->root->term, edge);
                }
            }
            Agnode_t* n = agnode(g, const_cast<char*>(pair.first->term.c_str()), TRUE);
            if (!pair.second.empty()) {
                agsafeset(n, const_cast<char*>("color"), const_cast<char*>("red"), const_cast<char*>(""));
            }

            for (conceptnet::Edge* edge : pair.second) {
                if (adjectiveAntonymMap.find(edge->fromConcept) == adjectiveAntonymMap.end() ||
                        adjectiveAntonymMap.find(edge->toConcept) == adjectiveAntonymMap.end()) {
                    continue;
                }
                generateEdge(g, openNodes, pair.first->term, edge);
            }
        }
    }
}

void AnswerGraph::generateEdge(
        Agraph_t* g, std::vector<conceptnet::Concept*>& openNodes, const std::string& term, const conceptnet::Edge* edge)
{
    Agnode_t* to;
    Agnode_t* from;
    if (edge->fromConcept->term == term) {
        to = agnode(g, const_cast<char*>(edge->toConcept->term.c_str()), TRUE);
        from = agnode(g, const_cast<char*>(term.c_str()), TRUE);
        openNodes.push_back(edge->toConcept);
    } else {
        to = agnode(g, const_cast<char*>(term.c_str()), TRUE);
        from = agnode(g, const_cast<char*>(edge->fromConcept->term.c_str()), TRUE);
        openNodes.push_back(edge->fromConcept);
    }
    Agedge_t* ed = agedge(g, from, to, const_cast<char*>(conceptnet::relations[edge->relation]), TRUE);
    agsafeset(ed, const_cast<char*>("label"),
            const_cast<char*>(std::string(conceptnet::relations[edge->relation])
                                      .append(" / " + std::to_string((int) ((edge->weight + edge->level + edge->relatedness) * 100.0)))
                                      .c_str()),
            const_cast<char*>(""));
}

conceptnet::Concept* AnswerGraph::getConcept(std::string conceptId) const
{
    auto mapEntry = this->concepts.find(conceptId);
    if (mapEntry != this->concepts.end()) {
        return mapEntry->second;
    } else {
        return nullptr;
    }
}

conceptnet::Concept* AnswerGraph::createConcept(std::string conceptId, std::string term, std::string senseLabel)
{
    auto mapEntry = this->concepts.find(conceptId);
    if (mapEntry != this->concepts.end()) {
        return mapEntry->second;
    } else {
        return this->concepts.emplace(conceptId, new conceptnet::Concept(conceptId, term, senseLabel)).first->second;
    }
}

conceptnet::Edge* AnswerGraph::getEdge(std::string edgeId) const
{
    auto mapEntry = this->edges.find(edgeId);
    if (mapEntry != this->edges.end()) {
        return mapEntry->second;
    } else {
        return nullptr;
    }
}

std::vector<conceptnet::Edge*> AnswerGraph::getEdges(conceptnet::Concept* firstConcept, conceptnet::Concept* secondConcept)
{
    std::vector<conceptnet::Edge*> ret;
    for (auto& pair : this->edges) {
        if (pair.second->fromConcept == firstConcept && pair.second->toConcept == secondConcept) {
            ret.push_back(pair.second);
        }
        if (pair.second->toConcept == firstConcept && pair.second->fromConcept == secondConcept) {
            ret.push_back(pair.second);
        }
    }
    return ret;
}

conceptnet::Edge* AnswerGraph::createEdge(std::string edgeId, std::string language, conceptnet::Concept* fromConcept,
        conceptnet::Concept* toConcept, conceptnet::Relation relation, double weight)
{
    auto mapEntry = this->edges.find(edgeId);
    if (mapEntry != this->edges.end()) {
        return mapEntry->second;
    } else {
        return this->edges.emplace(edgeId, new conceptnet::Edge(edgeId, language, fromConcept, toConcept, relation, weight)).first->second;
    }
}

const std::map<std::string, conceptnet::Concept*>& AnswerGraph::getConcepts() const
{
    return this->concepts;
}

const std::map<std::string, conceptnet::Edge*>& AnswerGraph::getEdges() const
{
    return this->edges;
}

long AnswerGraph::getNextASPID()
{
    this->counter++;
    return this->counter;
}

void AnswerGraph::updateASPID(long id) {
    if(id > this->counter) {
        this->counter = id;
    }
}

void AnswerGraph::removeEdge(std::string fromConceptString, std::string relation, std::string toConceptString)
{
    std::string edgeId = OntologyExtractor::generateEdgeId(fromConceptString, relation, toConceptString);
    conceptnet::Edge* edge = this->edges.at(edgeId);
    for (auto pair : this->concepts) {
        pair.second->removeEdge(edge);
    }
    this->edges.erase(edgeId);
    delete edge;
}

void AnswerGraph::resetASPID()
{
    this->counter = 0;
}

} // namespace cnoe